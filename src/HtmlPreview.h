/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014, 2015 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#ifndef HTMLPREVIEWWIDGET_H
#define HTMLPREVIEWWIDGET_H

#include <QWidget>
#include <QTextDocument>
#include <QComboBox>
#include <QThread>
#include <QTimer>
#include <QList>
#include <QPrinter>
#include <QPushButton>
#include <QRegExp>
#include <QLayout>
#include <QGridLayout>
#include <QUrl>
#include <QFutureWatcher>
#include <QStringList>

#if QT_VERSION >= 0x050000
#include <QtWebKitWidgets>
#else
#include <QtWebKit>
#endif

#include "Exporter.h"
#include "TextDocument.h"

class QPrintPreviewDialog;
class QPrinter;
class QStatusBar;

/**
 * Live HTML Preview window.
 */
class HtmlPreview : public QWidget
{
    Q_OBJECT

    public:
        /**
         * Constructor.  Takes text document to be rendered as HTML as
         * parameter.
         */
        HtmlPreview(TextDocument* document_, QWidget* parent = 0);

        /**
         * Destructor.
         */
        virtual ~HtmlPreview();

        QStatusBar* statusBar_;

        QString getStyleSheet();
        QString getStyleSheetName() const;
        void print() const;
signals:
        /**
         * Emitted when a lengthy operation has started, such as when the user
         * chooses to export the document to disk, so that the user can be
         * informed, perhaps via a progress bar.  The description provides
         * descriptive text as to the nature of the operation to display to
         * the user.
         */
        void operationStarted(const QString& description);

        /**
         * Emitted when a lengthy operation has finished, such as when an
         * exportation of the document to disk has completed, so that the user
         * can be informed, perhaps via the removal of a progress bar previously
         * displayed when the operation began.
         */
        void operationFinished();

    public slots:
        /**
         * Call this method to re-render the HTML for the document.
         */
        void updatePreview();

        /**
         * Call this method to navigate to the HTML heading tag (h1 - h6)
         * having the given sequence number.  For example, to navigate to the
         * very first heading in the document, pass in a value of 1.  To go
         * to the second heading that appears in the document, pass in a value
         * of 2, etc.
         */
        void navigateToHeading(int headingSequenceNumber);

    private slots:
        void onHtmlReady();
        void changeStyleSheet(int index);
        void printPreview();
        void printHtmlToPrinter(QPrinter* printer);
        void onExport();
        void copyHtml();
        void onLinkClicked(const QUrl& url);

        /**
         * Sets the base directory path for determining resource
         * paths relative to the web page being previewed.
         * This method is called whenever the file path changes.
         */
        void updateBaseDir();

    protected:
        QSize sizeHint() const;
        void closeEvent(QCloseEvent* event);

    private:
        QWebView* htmlBrowser_;
        QUrl baseUrl_;
        TextDocument* document_;
        QGridLayout* preferredLayout_;
        QComboBox* styleSheetComboBox_;
        Exporter* exporter_;
        QTimer* htmlPreviewUpdateTimer_;
        int minWidth_;
        bool documentChanged_;
        bool typingPaused_;
        QString html_;
        QRegExp headingTagExp_;
        int lastStyleSheetIndex_;
        QStringList customCssFiles_;

        /*
         * Used to set default page layout options for printing.  Also,
         * if the user closes the print preview dialog, the page layout and
         * page size settings are remembered in the event that the user reopens
         * the dialog during the same application session.
         */
        QPrinter printer_;

        // flag used to prevent recursion in changeStyleSheet
        bool handlingStyleSheetChange_;

        QFutureWatcher<QString>* futureWatcher_;
        QStringList defaultStyleSheets_;

        /*
         * Sets the HTML contents to display, and creates a backup of the old
         * HTML for diffing to scroll to the first difference whenever
         * updatePreview() is called.
         */
        void setHtml(const QString& html_);

        QString exportToHtml(const QString& text, Exporter* exporter_) const;

        void updateStyle();
};

#endif // HTMLPREVIEWWIDGET_H
