/***********************************************************************
 *
 * Copyright (C) 2014, 2015 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include <QFileInfo>
#include <QList>
#include <QString>
#include <QApplication>
#include <QFileDialog>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QComboBox>
#include <QCheckBox>
#include <QLabel>
#include <QSettings>
#include <QDesktopServices>
#include <QUrl>

#include "MainWindow.h"
#include "ExportDialog.h"
#include "ExporterFactory.h"
#include "Exporter.h"
#include "MessageBoxHelper.h"

#define GW_LAST_EXPORTER_KEY "Export/lastUsedExporter"
#define GW_SMART_TYPOGRAPHY_KEY "Export/smartTypographyEnabled"

ExportDialog::ExportDialog(TextDocument* document, HtmlPreview* preview, QWidget* parent) :
    QDialog{parent},
    htmlPreview_{preview},
    document_{document}
{
    QList<Exporter*> exporters{ExporterFactory::getInstance()->getFileExporters()};

    this->setWindowTitle(tr("Export"));
    fileDialogWidget_ = new QFileDialog{this, Qt::Widget};
    fileDialogWidget_->setAcceptMode(QFileDialog::AcceptSave);
    fileDialogWidget_->setFileMode(QFileDialog::AnyFile);
    fileDialogWidget_->setWindowFlags(Qt::Widget);

    QList<QUrl> shortcutFolders{};

#if QT_VERSION >= 0x050000
    QStringList standardLocations =
        QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    standardLocations <<
        QStandardPaths::standardLocations(QStandardPaths::DesktopLocation);
    standardLocations <<
        QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);

    // Adds the "My Computer" shortcut.
    shortcutFolders << QUrl("file:");

    foreach (QString loc, standardLocations)
    {
        shortcutFolders << QUrl::fromLocalFile(loc);
    }

#else
    shortcutFolders
        << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::HomeLocation))
        << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DesktopLocation))
        << QUrl::fromLocalFile(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation));
#endif

    // In Qt 5, the QFileDialog will be scrunched unless setVisible is called.
    // Also, Qt 5 also will not show custom sidebar URLs unless setVisible() is
    // called first.  Note that this is not an issue in Qt 4.
    //
    fileDialogWidget_->setVisible(true);

    fileDialogWidget_->setSidebarUrls(shortcutFolders);

    connect(fileDialogWidget_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(fileDialogWidget_, SIGNAL(rejected()), this, SLOT(reject()));

    exporterComboBox_ = new QComboBox{};
    int selectedIndex{0};

    QSettings settings{};
    QString lastExporterName{settings.value(GW_LAST_EXPORTER_KEY, QString()).toString()};

    for (int i{0}; i < exporters.length(); ++i) {
        const Exporter* exporter{exporters[i]};

        exporterComboBox_->addItem(exporter->getName(),
                                  qVariantFromValue(static_cast<void *>(exporters[i])));

        QList<const ExportFormat*> formats{exporter->getSupportedFormats()};

        // Build file filter list
        QString filter{""};

        for (int j{0}; j < formats.length(); ++j) {
            filter += formats[j]->getNamedFilter();

            if ((j + 1) < formats.length())
                filter += QString(";;");
        }

        fileFilters_.append(filter);

        if (exporter->getName() == lastExporterName)
            selectedIndex = i;
    }

    exporterComboBox_->setCurrentIndex(selectedIndex);
    fileDialogWidget_->setNameFilter(fileFilters_[selectedIndex]);

    QString initialDirPath{};
    QString baseName{};

    if (!document->getFilePath().isNull() && !document->getFilePath().isEmpty()) {
        QFileInfo inputFileInfo{document->getFilePath()};
        initialDirPath = inputFileInfo.dir().path();
        baseName = inputFileInfo.baseName();
    } else {
#if QT_VERSION >= 0x050000
        standardLocations =
            QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);

        if (standardLocations.size() > 0)
        {
            initialDirPath = standardLocations[0];
        }
#else
        initialDirPath =
            QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation);
#endif
    }

    fileDialogWidget_->setDirectory(initialDirPath);

    if (exporters[selectedIndex]->getSupportedFormats().length() > 0) {
        const ExportFormat* currentFormat{exporters[selectedIndex]->getSupportedFormats().at(0)};
        QString fileSuffix{currentFormat->getDefaultFileExtension()};

        if (!fileSuffix.isNull() && !fileSuffix.isEmpty()) {
            if (!baseName.isNull()) {
                fileDialogWidget_->selectFile(initialDirPath + "/" +
                    baseName + "." + fileSuffix);
            }
        }

        if (currentFormat->isFileExtensionMandatory())
            fileDialogWidget_->setDefaultSuffix(currentFormat->getDefaultFileExtension());
    }

    const bool smartTypographyEnabled{settings.value(GW_SMART_TYPOGRAPHY_KEY, true).toBool()};
    smartTypographyCheckBox_ = new QCheckBox{tr("Smart Typography")};
    smartTypographyCheckBox_->setChecked(smartTypographyEnabled);

    QVBoxLayout* layout{new QVBoxLayout{this}};
    layout->addWidget(fileDialogWidget_);
    layout->addWidget(smartTypographyCheckBox_);
    this->setLayout(layout);

    connect(exporterComboBox_, SIGNAL(currentIndexChanged(int)), this, SLOT(onExporterChanged(int)));
    connect(fileDialogWidget_, SIGNAL(filterSelected(QString)), this, SLOT(onFilterSelected(QString)));
}

ExportDialog::~ExportDialog()
{
    QSettings settings{};
    settings.setValue(GW_LAST_EXPORTER_KEY, exporterComboBox_->currentText());
    settings.setValue(GW_SMART_TYPOGRAPHY_KEY, smartTypographyCheckBox_->isChecked());
}

void ExportDialog::accept()
{
    QDialog::accept();
    this->setResult(fileDialogWidget_->result());

    const QStringList selectedFiles{fileDialogWidget_->selectedFiles()};

    if (!selectedFiles.isEmpty()) {
        const QString fileName{selectedFiles.at(0)};
        const QString selectedFilter{fileDialogWidget_->selectedNameFilter()};
        const int selectedIndex{exporterComboBox_->currentIndex()};
        const QVariant exporterVariant{exporterComboBox_->itemData(selectedIndex)};
        Exporter* exporter{static_cast<Exporter*>(exporterVariant.value<void*>())};

        for (const ExportFormat* format : exporter->getSupportedFormats()) {
            if (format->getNamedFilter() == selectedFilter) {
                QString err{};

                QApplication::setOverrideCursor(Qt::WaitCursor);
                emit exportStarted(tr("exporting to %1").arg(fileName));

                exporter->setSmartTypographyEnabled(smartTypographyCheckBox_->isChecked());
                exporter->exportToFile(
                    format,
                    this->document_->getFilePath(),
                    document_->toPlainText(),
                    htmlPreview_->getStyleSheet(),
                    fileName,
                    err);

                emit exportComplete();
                QApplication::restoreOverrideCursor();

                if (!err.isNull())
                    MessageBoxHelper::critical(this, tr("Export failed."), err);

                break;
            }
        }
    }
}

void ExportDialog::reject()
{
    QDialog::reject();
    this->setResult(fileDialogWidget_->result());
}

void ExportDialog::onExporterChanged(int index)
{
    QVariant exporterVariant{exporterComboBox_->itemData(index)};
    Exporter* exporter{static_cast<Exporter*>(exporterVariant.value<void*>())};

    if (exporter->getSupportedFormats().length() > 0) {
        const ExportFormat* format{exporter->getSupportedFormats().at(0)};

        if (format->isFileExtensionMandatory())
            fileDialogWidget_->setDefaultSuffix(format->getDefaultFileExtension());
    }

    fileDialogWidget_->setNameFilter(fileFilters_[index]);
}

void ExportDialog::onFilterSelected(const QString& filter)
{
    int selectedIndex{exporterComboBox_->currentIndex()};
    QVariant exporterVariant{exporterComboBox_->itemData(selectedIndex)};
    Exporter* exporter{static_cast<Exporter*>(exporterVariant.value<void*>())};

    for (const ExportFormat* format : exporter->getSupportedFormats()) {
        if (format->getNamedFilter() == filter) {
            if (format->isFileExtensionMandatory())
                fileDialogWidget_->setDefaultSuffix(format->getDefaultFileExtension());

            break;
        }
    }
}
