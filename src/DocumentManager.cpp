/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014, 2015 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include <QTextDocument>
#include <QPair>
#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>
#include <QFileSystemWatcher>
#include <QString>
#include <QFileInfo>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QFileDialog>
#include <QPrinter>
#include <QPrintPreviewDialog>
#include <QPrintDialog>
#include <QTimer>
#include <QApplication>

#include "MainWindow.h"
#include "DocumentManager.h"
#include "DocumentHistory.h"
#include "MarkdownEditor.h"
#include "Exporter.h"
#include "ExporterFactory.h"
#include "MarkdownHighlighter.h"
#include "MarkdownTokenizer.h"
#include "ExportDialog.h"
#include "MessageBoxHelper.h"
#include "ThemeFactory.h"

const QString DocumentManager::FILE_CHOOSER_FILTER{
    QString("%1 (*.md *.markdown *.txt);;%2 (*.txt);;%3 (*)")
            .arg(QObject::tr("Markdown"))
            .arg(QObject::tr("Text"))
            .arg(QObject::tr("All"))};

DocumentManager::DocumentManager(MarkdownEditor* editor, HtmlPreview* preview, QWidget* parent) :
    QObject(parent),
    htmlPreview_{preview},
    parentWidget_{parent},
    editor_{editor},
    fileHistoryEnabled_{true},
    saveInProgress_{false}
{
    saveFutureWatcher_ = new QFutureWatcher<QString>(this);

    fileWatcher_ = new QFileSystemWatcher(this);
    document_ = static_cast<TextDocument*>(editor->document());

    connect(document_, SIGNAL(modificationChanged(bool)), this, SLOT(onDocumentModifiedChanged(bool)));

    // Set up default page layout and page size for printing.
    printer_.setPaperSize(QPrinter::Letter);
    printer_.setPageMargins(0.5, 0.5, 0.5, 0.5, QPrinter::Inch);

    connect(saveFutureWatcher_, SIGNAL(finished()), this, SLOT(onSaveCompleted()));
    connect(fileWatcher_, SIGNAL(fileChanged(QString)), this, SLOT(onFileChangedExternally(QString)));
}

DocumentManager::~DocumentManager()
{
    this->saveFutureWatcher_->waitForFinished();
}

TextDocument* DocumentManager::getDocument() const
{
    return this->document_;
}

void DocumentManager::setFileHistoryEnabled(bool enabled)
{
    fileHistoryEnabled_ = enabled;
}

void DocumentManager::open(const QString& filePath)
{
    if (checkSaveChanges()) {
        QString path{};

        if (!filePath.isNull() && !filePath.isEmpty()) {
            path = filePath;
        } else {
            QString startingDirectory{};

            if (!document_->isNew())
                startingDirectory = QFileInfo(document_->getFilePath()).dir().path();

            path = QFileDialog::getOpenFileName(
                    parentWidget_,
                    tr("Open File"),
                    startingDirectory,
                    FILE_CHOOSER_FILTER);
        }

        if (!path.isNull() && !path.isEmpty()) {
            QFileInfo fileInfo{path};

            if (!fileInfo.isReadable()) {
                MessageBoxHelper::critical(
                    parentWidget_,
                    tr("Could not open %1").arg(path),
                    tr("Permission denied."));

                return;
            }

            QString oldFilePath{document_->getFilePath()};
            int oldCursorPosition{editor_->textCursor().position()};
            QString oldStyleSheet{static_cast<MainWindow*>(parent())->getStyleSheetName()};
            bool oldFileWasNew{document_->isNew()};

            if (!loadFile(path)) {
                // The error dialog should already have been displayed
                // in loadFile().
                //
                return;
            } else if (oldFilePath == document_->getFilePath()) {
                editor_->navigateDocument(oldCursorPosition);
            } else if (fileHistoryEnabled_) {
                if (!oldFileWasNew) {
                    DocumentHistory history{};
                    history.add(oldFilePath, oldCursorPosition, oldStyleSheet);
                }

                // Always emit a documentClosed() signal, even if the document
                // was new and untitled.  This is so that if a file from the
                // displayed history is being loaded, its path will be
                // guaranteed to be removed from the "Open Recent" file list
                // displayed to the user (because it's already opened).
                //
                emit documentClosed();
            }
        }
    }
}

void DocumentManager::reopenLastClosedFile()
{
    if (fileHistoryEnabled_) {
        DocumentHistory history{};
        QStringList recentFiles{history.getRecentFiles(2)};

        if (!document_->isNew()) {
            recentFiles.removeAll(document_->getFilePath());
        }

        if (!recentFiles.isEmpty()) {
            open(recentFiles.first());
            emit documentClosed();
        }
    }
}

void DocumentManager::reload()
{
    if (!document_->isNew()) {
        if (document_->isModified()) {
            // Prompt user if he wants to save changes.
            int response{MessageBoxHelper::question(
                            parentWidget_,
                            tr("The document has been modified."),
                            tr("Discard changes?"),
                            QMessageBox::Yes | QMessageBox::No,
                            QMessageBox::No)};

            if (QMessageBox::No == response)
                return;
        }

        QTextCursor cursor{editor_->textCursor()};
        int pos{cursor.position()};

        if (loadFile(document_->getFilePath())) {
            cursor.setPosition(pos);
            editor_->setTextCursor(cursor);
        }
    }
}

void DocumentManager::rename()
{
    if (document_->isNew()) {
        saveAs();
    } else {
        QString filePath{QFileDialog::getSaveFileName(
                        parentWidget_,
                        tr("Rename File"),
                        QString(),
                        FILE_CHOOSER_FILTER)};

        if (!filePath.isNull() && !filePath.isEmpty()) {
            QFile file{document_->getFilePath()};
            bool success{file.rename(filePath)};

            if (!success) {
                MessageBoxHelper::critical(
                    parentWidget_,
                    tr("Failed to rename %1").arg(document_->getFilePath()),
                    file.errorString());
                return;
            }

            setFilePath(filePath);
            save();
        }
    }
}

bool DocumentManager::save()
{
    if (document_->isNew()) {
        return saveAs();
    } else if (checkPermissionsBeforeSave()) {
        document_->setModified(false);
        emit documentModifiedChanged(false);

        if (this->saveFutureWatcher_->isRunning() || this->saveFutureWatcher_->isStarted())
            this->saveFutureWatcher_->waitForFinished();

        saveInProgress_ = true;

        if (fileWatcher_->files().contains(document_->getFilePath()))
            this->fileWatcher_->removePath(document_->getFilePath());

        document_->setTimestamp(QDateTime::currentDateTime());

        QFuture<QString> future{
            QtConcurrent::run(this,
                              &DocumentManager::saveToDisk,
                              document_->getFilePath(),
                              document_->toPlainText())};

        this->saveFutureWatcher_->setFuture(future);
        return true;
    }

    return false;
}

bool DocumentManager::saveAs()
{
    QString startingDirectory{};

    if (!document_->isNew())
        startingDirectory = QFileInfo{document_->getFilePath()}.dir().path();

    QString filePath{QFileDialog::getSaveFileName(
                    parentWidget_,
                    tr("Save File"),
                    startingDirectory,
                    FILE_CHOOSER_FILTER)};

    if (!filePath.isNull() && !filePath.isEmpty()) {
        setFilePath(filePath);
        return save();
    }

    return false;
}

bool DocumentManager::close()
{
    if (checkSaveChanges()) {
        if (saveFutureWatcher_->isRunning() || saveFutureWatcher_->isStarted())
            this->saveFutureWatcher_->waitForFinished();

        // Get the document's information before closing it out
        // so we can store history information about it.
        //
        QString filePath{document_->getFilePath()};
        int cursorPosition{editor_->textCursor().position()};
        QString styleSheet{static_cast<MainWindow*>(parent())->getStyleSheetName()};
        bool documentIsNew{document_->isNew()};

        // Set up a new, untitled document.  Note that the document
        // needs to be wiped clean before emitting the documentClosed()
        // signal, because slots accepting this signal may check the
        // new (replacement) document's status.
        //
        document_->setPlainText("");
        document_->clearUndoRedoStacks();
        editor_->setReadOnly(false);
        document_->setReadOnly(false);
        setFilePath(QString{});
        document_->setModified(false);

        if (fileHistoryEnabled_ && !documentIsNew) {
            DocumentHistory history{};
            history.add(filePath, cursorPosition, styleSheet);

            emit documentClosed();
        }

        return true;
    }

    return false;
}

void DocumentManager::exportFile()
{
    ExportDialog exportDialog{document_, htmlPreview_};

    connect(&exportDialog, SIGNAL(exportStarted(QString)), this, SIGNAL(operationStarted(QString)));
    connect(&exportDialog, SIGNAL(exportComplete()), this, SIGNAL(operationFinished()));

    exportDialog.exec();
}

//void DocumentManager::printPreview()
//{
//    QPrintPreviewDialog printPreviewDialog{&printer_, parentWidget_};
//    connect(&printPreviewDialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(printFileToPrinter(QPrinter*)));
//    printPreviewDialog.exec();
//}

//void DocumentManager::print()
//{
//    QPrintDialog printDialog{&printer_, parentWidget_};

//    if (printDialog.exec() == QDialog::Accepted)
//        printFileToPrinter(&printer_);
//}

//void DocumentManager::printFileToPrinter(QPrinter* printer)
//{
//    htmlPreview_->print(printer);


////Old print
////    QString text{editor->document()->toPlainText()};
////    QTextDocument doc{text};

////    MarkdownHighlighter highlighter{&doc};
////    Theme printerTheme{ThemeFactory::getInstance()->getPrinterFriendlyTheme()};
////    highlighter.setColorScheme(
////                printerTheme.getDefaultTextColor(),
////                printerTheme.getSelectionColor(),
////                printerTheme.getCursorColor(),
////                printerTheme.getBackgroundColor(),
////                printerTheme.getMarkdownColor(),
////                printerTheme.getLinkColor(),
////                printerTheme.getSpellingErrorColor());

////    highlighter.setSpellCheckEnabled(false);
////    highlighter.setFont(editor->font().family(), editor->font().pointSizeF());
////    doc.print(printer);
//}

void DocumentManager::onDocumentModifiedChanged(bool modified)
{
    if (document_->isNew() || document_->isReadOnly())
        emit documentModifiedChanged(modified);
}

void DocumentManager::onSaveCompleted()
{
    QString err{this->saveFutureWatcher_->result()};

    if (!err.isNull() && !err.isEmpty()) {
        MessageBoxHelper::critical(
            parentWidget_,
            tr("Error saving %1").arg(document_->getFilePath()),
            err);
    } else if (!fileWatcher_->files().contains(document_->getFilePath())) {
        fileWatcher_->addPath(document_->getFilePath());
    }

    saveInProgress_ = false;
}

void DocumentManager::onFileChangedExternally(const QString& path)
{
    QFileInfo fileInfo{path};

    if (!fileInfo.exists()) {
        emit documentModifiedChanged(true);

        // Make sure autosave knows the document is modified so it can
        // save it.
        //
        document_->setModified(true);
    } else {
        if (fileInfo.isWritable() && document_->isReadOnly()) {
            document_->setReadOnly(false);
        } else {
            if (!fileInfo.isWritable() && !document_->isReadOnly()) {
                document_->setReadOnly(true);

                if (document_->isModified())
                    emit documentModifiedChanged(true);
            }
        }

        // Need to guard against the QFileSystemWatcher from signalling a
        // file change when we're the one who changed the file by saving.
        // Thus, check the saveInProgress flag before prompting.
        //
        if (!saveInProgress_ && (fileInfo.lastModified() > document_->getTimestamp())) {
            int response{MessageBoxHelper::question(
                            parentWidget_,
                            tr("The document has been modified by another program."),
                            tr("Would you like to reload the document?"),
                            QMessageBox::Yes | QMessageBox::No,
                            QMessageBox::Yes)};

            if (QMessageBox::Yes == response)
                reload();
        }
    }
}

bool DocumentManager::loadFile(const QString& filePath)
{
    QFileInfo fileInfo{filePath};
    QFile inputFile{filePath};

    if (!inputFile.open(QIODevice::ReadOnly)) {
        MessageBoxHelper::critical(
            parentWidget_,
            tr("Could not read %1").arg(filePath),
            inputFile.errorString());
        return false;
    }

    document_->clearUndoRedoStacks();
    document_->setUndoRedoEnabled(false);
    document_->setPlainText("");

    QTextCursor cursor{document_};
    cursor.setPosition(0);

    QApplication::setOverrideCursor(Qt::WaitCursor);
    emit operationStarted(tr("opening %1").arg(filePath));
    QTextStream inStream{&inputFile};

    // Markdown files need to be in UTF-8 format, so assume that is
    // what the user is opening by default.  Enable autodection
    // of of UTF-16 or UTF-32 BOM in case the file isn't UTF-8 encoded.
    //
    inStream.setCodec("UTF-8");
    inStream.setAutoDetectUnicode(true);

    QString text{inStream.read(2048L)};

    while (!text.isNull()) {
        cursor.insertText(text);
        text = inStream.read(2048L);
    }

    document_->setUndoRedoEnabled(true);

    if (QFile::NoError != inputFile.error()) {
        MessageBoxHelper::critical(
            parentWidget_,
            tr("Could not read %1").arg(filePath),
            inputFile.errorString());

        inputFile.close();
        return false;
    }

    inputFile.close();

    if (fileHistoryEnabled_) {
        DocumentHistory history{};
        editor_->navigateDocument(history.getCursorPosition(filePath));
    } else {
        editor_->navigateDocument(0);
    }

    setFilePath(filePath);
    editor_->setReadOnly(false);

    if (!fileInfo.isWritable()) {
        document_->setReadOnly(true);
    } else {
        document_->setReadOnly(false);
    }

    document_->setModified(false);
    document_->setTimestamp(fileInfo.lastModified());

    fileWatcher_->addPath(filePath);
    emit operationFinished();
    emit documentModifiedChanged(false);
    QApplication::restoreOverrideCursor();

    editor_->centerCursor();

    return true;
}

void DocumentManager::setFilePath(const QString& filePath)
{
    if (!document_->isNew())
        fileWatcher_->removePath(document_->getFilePath());

    document_->setFilePath(filePath);

    if (!filePath.isNull() && !filePath.isEmpty()) {
        QFileInfo fileInfo{filePath};

        if (fileInfo.exists()) {
            document_->setReadOnly(!fileInfo.isWritable());
        } else {
            document_->setReadOnly(false);
        }
    } else {
        document_->setReadOnly(false);
    }

    emit documentDisplayNameChanged(document_->getDisplayName());
}

bool DocumentManager::checkSaveChanges()
{
    if (document_->isModified()) {
        // Prompt user if he wants to save changes.
        QString text{};

        if (document_->isNew()) {
            text = tr("File has been modified.");
        } else {
            text = (tr("%1 has been modified.")
                    .arg(document_->getDisplayName()));
        }

        int response {MessageBoxHelper::question(
                parentWidget_,
                text,
                tr("Would you like to save your changes?"),
                QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
                QMessageBox::Save)};

        switch (response) {
        case QMessageBox::Save:
            if (document_->isNew()) {
                return saveAs();
            } else {
                return save();
            }
            break;
        case QMessageBox::Cancel:
            return false;
        default:
            break;
        }
    }
    return true;
}

bool DocumentManager::checkPermissionsBeforeSave()
{
    if (document_->isReadOnly()) {
        int response {MessageBoxHelper::question(
                parentWidget_,
                tr("%1 is read only.").arg(document_->getFilePath()),
                tr("Overwrite protected file?"),
                QMessageBox::Yes | QMessageBox::No,
                QMessageBox::Yes)};

        if (QMessageBox::No == response) {
            return saveAs();
        } else {
            QFile file{document_->getFilePath()};
            fileWatcher_->removePath(document_->getFilePath());

            if (!file.remove()) {
                if (file.setPermissions(QFile::WriteUser | QFile::ReadUser) && file.remove()) {
                    document_->setReadOnly(false);
                    return true;
                } else {
                    MessageBoxHelper::critical(
                        parentWidget_,
                        tr("Overwrite failed."),
                        tr("Please save file to another location."));

                    fileWatcher_->addPath(document_->getFilePath());
                    return false;
                }
            } else {
                document_->setReadOnly(false);
            }
        }
    }

    return true;
}

QString DocumentManager::saveToDisk(const QString& filePath, const QString& text) const
{
    QString err{};

    if (filePath.isNull() || filePath.isEmpty())
        return QObject::tr("Null or empty file path provided for writing.");

    QFile outputFile{filePath};

    if (!outputFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
        return outputFile.errorString();

    // Write contents to disk.
    QTextStream outStream{&outputFile};

    // Markdown files need to be in UTF-8, since most Markdown processors
    // (i.e., Pandoc, et. al.) can only read UTF-8 encoded text files.
    //
    outStream.setCodec("UTF-8");
    outStream << text;

    if (QFile::NoError != outputFile.error())
        err = outputFile.errorString();

    // Close the file.  All done!
    outputFile.close();
    return err;
}
