/***********************************************************************
 *
 * Copyright (C) 2014, 2015 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include <QBrush>
#include <QColor>
#include <QFont>
#include <QObject>
#include <QRegExp>
#include <QString>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextBlockFormat>
#include <QStyle>
#include <QApplication>
#include <Qt>

#include "MarkdownHighlighter.h"
#include "MarkdownTokenizer.h"
#include "MarkdownTokenTypes.h"
#include "MarkdownStates.h"
#include "ColorHelper.h"
#include "spelling/dictionary_ref.h"
#include "spelling/dictionary_manager.h"

#define GW_FADE_ALPHA 192

MarkdownHighlighter::MarkdownHighlighter(QTextDocument* document)
    : QSyntaxHighlighter(document),
      tokenizer{NULL},
      dictionary{DictionaryManager::instance().requestDictionary()},
      spellCheckEnabled{false},
      useUndlerlineForEmphasis{false},
      inBlockquote{false},
      defaultTextColor_{Qt::black},
      backgroundColor_{Qt::white},
      markupColor_{Qt::black},
      linkColor_{Qt::blue},
      spellingErrorColor_{Qt::red}
{
    this->tokenizer = new MarkdownTokenizer();

    connect(
        this,
        SIGNAL(highlightBlockAtPosition(int)),
        this,
        SLOT(onHighlightBlockAtPosition(int)),
        Qt::QueuedConnection);
    

    QFont font{};
    font.setFamily("Monospace");
    font.setWeight(QFont::Normal);
    font.setItalic(false);
    font.setPointSizeF(12.0);
    font.setStyleStrategy(QFont::PreferAntialias);
    defaultFormat.setFont(font);
    defaultFormat.setForeground(QBrush(defaultTextColor_));

    setupTokenColors();

    for (int i{0}; i < TokenLast; ++i) {
        applyStyleToMarkup[i] = false;
        emphasizeToken[i] = false;
        strongToken[i] = false;
        strongMarkup[i] = false;
        strikethroughToken[i] = false;
    }

    for (int i{TokenAtxHeading1}; i <= TokenAtxHeading6; ++i) {
        applyStyleToMarkup[i] = true;
    }
    
    applyStyleToMarkup[TokenEmphasis] = true;
    applyStyleToMarkup[TokenStrong] = true;
    applyStyleToMarkup[TokenAtxHeading1] = true;
    applyStyleToMarkup[TokenAtxHeading2] = true;
    applyStyleToMarkup[TokenAtxHeading3] = true;
    applyStyleToMarkup[TokenAtxHeading4] = true;
    applyStyleToMarkup[TokenAtxHeading5] = true;
    applyStyleToMarkup[TokenAtxHeading6] = true;

    emphasizeToken[TokenEmphasis] = true;
    emphasizeToken[TokenBlockquote] = false;
    strongToken[TokenStrong] = true;
    strongToken[TokenMention] = true;
    strongToken[TokenAtxHeading1] = true;
    strongToken[TokenAtxHeading2] = true;
    strongToken[TokenAtxHeading3] = true;
    strongToken[TokenAtxHeading4] = true;
    strongToken[TokenAtxHeading5] = true;
    strongToken[TokenAtxHeading6] = true;
    strongToken[TokenSetextHeading1Line1] = true;
    strongToken[TokenSetextHeading2Line1] = true;
    strongToken[TokenSetextHeading1Line2] = true;
    strongToken[TokenSetextHeading2Line2] = true;
    strongToken[TokenTableHeader] = true;
    strikethroughToken[TokenStrikethrough] = true;

    strongMarkup[TokenNumberedList] = true;
    strongMarkup[TokenBlockquote] = true;
    strongMarkup[TokenBulletPointList] = true;
}
        
MarkdownHighlighter::~MarkdownHighlighter()
{
    if (NULL != tokenizer) {
        delete tokenizer;
        tokenizer = NULL;
    }
}

// Note:  Never, ever set the QTextBlockFormat for a QTextBlock from within the
// highlighter.  Depending on how the block format is modified, a recursive call
// to the highlighter may be triggered, which will cause the application to
// crash.
//
// Likewise, don't try to set the QTextBlockFormat outside the highlighter
// (i.e., from within the text editor).  While the application will not crash,
// the format change will be added to the undo stack.  Attempting to undo from
// that point on will cause the undo stack to be virtually frozen, since undoing
// the format operation causes the text to be considered changed, thus
// triggering the slot that changes the text formatting to be triggered yet
// again.
//
void MarkdownHighlighter::highlightBlock(const QString& text)
{
    int lastState{currentBlockState()};

    setFormat(0, text.length(), defaultFormat);

    if (NULL != tokenizer) {
        tokenizer->clear();
        
        QTextBlock block{this->currentBlock()};
        int nextState{MarkdownStateUnknown};
        int previousState{this->previousBlockState()};

        if (block.next().isValid()) {
            nextState = block.next().userState();
        }

        tokenizer->tokenize(text, lastState, previousState, nextState);
        setCurrentBlockState(tokenizer->getState());

        if (MarkdownStateBlockquote == tokenizer->getState())
            inBlockquote = true;
        else
            inBlockquote = false;

            QList<Token> tokens{tokenizer->getTokens()};

        for (Token t : tokens){
            switch (t.getType()) {
                case TokenAtxHeading1:
                case TokenAtxHeading2:
                case TokenAtxHeading3:
                case TokenAtxHeading4:
                case TokenAtxHeading5:
                case TokenAtxHeading6:
                    applyFormattingForToken(t);
                    emit headingFound
                    (
                        block.position(),
                        t.getType() - TokenAtxHeading1 + 1,
                        text.mid
                            (
                                t.getPosition()
                                    + t.getOpeningMarkupLength(),
                                t.getLength()
                                    - t.getOpeningMarkupLength()
                                    - t.getClosingMarkupLength()
                            ).trimmed()
                    );
                    break;
                case TokenSetextHeading1Line1:
                    applyFormattingForToken(t);
                    emit headingFound(block.position(), 1, text);
                    break;
                case TokenSetextHeading2Line1:
                    applyFormattingForToken(t);
                    emit headingFound(block.position(), 2, text);
                    break;
                case TokenUnknown:
                    qWarning("Highlighter found unknown token type in text block.");
                    break;
                default:
                    applyFormattingForToken(t);
                    break;
            }
        }

        if (tokenizer->backtrackRequested()) {
            QTextBlock previous{currentBlock().previous()};
            emit highlightBlockAtPosition(previous.position());
        }
    }

    if (spellCheckEnabled)
        spellCheck(text);

    // If the block has transitioned from previously being a heading to now
    // being a non-heading, signal that the position in the document no longer
    // contains a heading.
    //
    if(isHeadingBlockState(lastState) && !isHeadingBlockState(currentBlockState()))
        emit headingRemoved(currentBlock().position());
}

void MarkdownHighlighter::setDictionary(const DictionaryRef& dictionary)
{
    this->dictionary = dictionary;

    if (spellCheckEnabled)
        rehighlight();
}

void MarkdownHighlighter::increaseFontSize()
{
    defaultFormat.setFontPointSize(defaultFormat.fontPointSize() + 1.0);
    rehighlight();
}

void MarkdownHighlighter::decreaseFontSize()
{
    defaultFormat.setFontPointSize(defaultFormat.fontPointSize() - 1.0);
    rehighlight();
}

void MarkdownHighlighter::setColorScheme(
        const QColor& defaultTextColor,
        const QColor& selectionColor,
        const QColor& cursorColor,
        const QColor& backgroundColor,
        const QColor& markupColor,
        const QColor& linkColor,
        const QColor& spellingErrorColor)
{
    defaultTextColor_ = defaultTextColor;
    selectionColor_ = selectionColor;
    cursorColor_ = cursorColor;
    backgroundColor_ = backgroundColor;
    markupColor_ = markupColor;
    linkColor_ = linkColor;
    spellingErrorColor_ = spellingErrorColor;
    defaultFormat.setForeground(QBrush(defaultTextColor));
    setupTokenColors();
    rehighlight();
}

void MarkdownHighlighter::setFont(const QString& fontFamily, const double fontSize)
{
    QFont font{};
    font.setFamily(fontFamily);
    font.setWeight(QFont::Normal);
    font.setItalic(false);
    font.setPointSizeF(fontSize);
    defaultFormat.setFont(font);
    rehighlight();
}

void MarkdownHighlighter::setSpellCheckEnabled(const bool enabled)
{
    spellCheckEnabled = enabled;
    rehighlight();
}

void MarkdownHighlighter::onHighlightBlockAtPosition(int position)
{
    QTextBlock block{document()->findBlock(position)};
    rehighlightBlock(block);
}

bool MarkdownHighlighter::isHeadingBlockState(int state) const
{
    switch (state) {
        case MarkdownStateAtxHeading1:
        case MarkdownStateAtxHeading2:
        case MarkdownStateAtxHeading3:
        case MarkdownStateAtxHeading4:
        case MarkdownStateAtxHeading5:
        case MarkdownStateAtxHeading6:
        case MarkdownStateSetextHeading1Line1:
        case MarkdownStateSetextHeading2Line1:
            return true;
        default:
            return false;
    }
}

void MarkdownHighlighter::spellCheck(const QString& text)
{
    QStringRef misspelledWord{dictionary.check(text, 0)};

    while (!misspelledWord.isNull()) {
        int startIndex{misspelledWord.position()};
        int length{misspelledWord.length()};

        QTextCharFormat spellingErrorFormat{format(startIndex)};
        spellingErrorFormat.setUnderlineColor(spellingErrorColor_);
        spellingErrorFormat.setUnderlineStyle(
            static_cast<QTextCharFormat::UnderlineStyle>(
            QApplication::style()->styleHint(QStyle::SH_SpellCheckUnderlineStyle)));

        setFormat(startIndex, length, spellingErrorFormat);

        startIndex += length;
        misspelledWord = dictionary.check(text, startIndex);
    }
}

void MarkdownHighlighter::setupTokenColors()
{
    for (int i{0}; i < TokenLast; ++i) {
        colorForToken[i] = defaultTextColor_;
    }

    QColor fadedColor{ColorHelper::applyAlpha(defaultTextColor_, backgroundColor_, GW_FADE_ALPHA)};

    colorForToken[TokenBlockquote] = fadedColor;
    colorForToken[TokenCodeBlock] = fadedColor;
    colorForToken[TokenVerbatim] = fadedColor;
    colorForToken[TokenHtmlTag] = markupColor_;
    colorForToken[TokenAtxHeading1] = markupColor_;
    colorForToken[TokenAtxHeading2] = markupColor_;
    colorForToken[TokenAtxHeading3] = markupColor_;
    colorForToken[TokenAtxHeading4] = markupColor_;
    colorForToken[TokenAtxHeading5] = markupColor_;
    colorForToken[TokenAtxHeading6] = markupColor_;
    colorForToken[TokenHtmlEntity] = markupColor_;
    colorForToken[TokenAutomaticLink] = linkColor_;
    colorForToken[TokenInlineLink] = linkColor_;
    colorForToken[TokenReferenceLink] = linkColor_;
    colorForToken[TokenReferenceDefinition] = linkColor_;
    colorForToken[TokenImage] = linkColor_;
    colorForToken[TokenMention] = linkColor_;
    colorForToken[TokenHtmlComment] = markupColor_;
    colorForToken[TokenHorizontalRule] = markupColor_;
    colorForToken[TokenGithubCodeFence] = markupColor_;
    colorForToken[TokenPandocCodeFence] = markupColor_;
    colorForToken[TokenCodeFenceEnd] = markupColor_;
    colorForToken[TokenSetextHeading1Line2] = markupColor_;
    colorForToken[TokenSetextHeading2Line2] = markupColor_;
    colorForToken[TokenTableDivider] = markupColor_;
    colorForToken[TokenTablePipe] = markupColor_;
}

void MarkdownHighlighter::applyFormattingForToken(const Token& token)
{
    if (TokenUnknown != token.getType()) {
        int tokenType{token.getType()};
        QTextCharFormat format{this->format(token.getPosition())};

        QColor tokenColor{colorForToken[tokenType]};

        if (inBlockquote && token.getType() != TokenBlockquote)
            tokenColor = ColorHelper::applyAlpha(tokenColor, backgroundColor_, GW_FADE_ALPHA);

        format.setForeground(QBrush(tokenColor));

        if (strongToken[tokenType])
            format.setFontWeight(QFont::Bold);

        if (emphasizeToken[tokenType]) {
            if (useUndlerlineForEmphasis && (tokenType != TokenBlockquote))
                format.setFontUnderline(true);
            else
                format.setFontItalic(true);
        }

        if (strikethroughToken[tokenType])
            format.setFontStrikeOut(true);

        format.setFontPointSize(format.fontPointSize());

        QTextCharFormat markupFormat{};

        if ( applyStyleToMarkup[tokenType] && (!emphasizeToken[tokenType] || !useUndlerlineForEmphasis))
            markupFormat = format;
        else
            markupFormat = this->format(token.getPosition());


        QColor adjustedMarkupColor{this->markupColor_};

        if (inBlockquote && token.getType() != TokenBlockquote)
            adjustedMarkupColor = ColorHelper::applyAlpha(adjustedMarkupColor, backgroundColor_, GW_FADE_ALPHA);

        markupFormat.setForeground(QBrush(adjustedMarkupColor));

        if (strongMarkup[tokenType])
            markupFormat.setFontWeight(QFont::Bold);

        if (token.getOpeningMarkupLength() > 0)
            setFormat(token.getPosition(), token.getOpeningMarkupLength(), markupFormat);

        setFormat(token.getPosition() + token.getOpeningMarkupLength(),
                  token.getLength() - token.getOpeningMarkupLength() - token.getClosingMarkupLength(),
                  format);

        if (token.getClosingMarkupLength() > 0) {
            setFormat(token.getPosition() + token.getLength() - token.getClosingMarkupLength(),
                      token.getClosingMarkupLength(),
                      markupFormat);
        }
    } else {
        qWarning("Highlighter::applyFormattingForToken() was passed in a "
            "token of unknown type.");
    }
}
