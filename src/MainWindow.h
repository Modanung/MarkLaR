/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014-2016 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QObject>
#include <QMainWindow>
#include <QWidget>
#include <QMenu>
#include <QAction>
#include <QLabel>
#include <QGraphicsColorizeEffect>
#include <QGraphicsDropShadowEffect>

#include "MarkdownEditor.h"
#include "HtmlPreview.h"
#include "ThemeFactory.h"
#include "AppSettings.h"
#include "EffectsMenuBar.h"
#include "find_dialog.h"
#include "spelling/dictionary_manager.h"
#include "spelling/dictionary_dialog.h"

#define MAX_RECENT_FILES 10

#define STYLIZED_NAME QString{"MarkLaR"}

class SpellBook;
class QSettings;
class QFileSystemWatcher;
class QTextBrowser;
class QCheckBox;
class DocumentManager;
class MarkdownHighlighter;
class Outline;

/**
 * Main window for the application.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

	public:
        MainWindow(const QString& filePath = QString(), QWidget* parent = 0);
        virtual ~MainWindow();

        Theme getTheme() const { return theme_; }
        QString getStyleSheetName() const { return htmlPreviewWidget_->getStyleSheetName(); }
        
protected:
        QSize sizeHint() const;
        void resizeEvent(QResizeEvent* event);
        void keyPressEvent(QKeyEvent* e);
        void paintEvent(QPaintEvent* event);
        void closeEvent(QCloseEvent* event);

    private slots:
        void quitApplication();
        void changeTheme();
        void showFindReplaceDialog();
        void toggleFullscreen(bool checked);
        void toggleHideMenuBarInFullScreen(bool checked);
        void toggleLiveSpellCheck(bool checked);
        void toggleFileHistoryEnabled(bool checked);
        void toggleAutoMatch(bool checked);
        void toggleBulletPointCycling(bool checked);
        void insertImage();
        void changeTabulationWidth();
        void showQuickReferenceGuide();
        void showEditor();
        void showOutline();
        void showSpellBook();
        void onQuickRefGuideLinkClicked(const QUrl& url);
        void showAbout();
        void updateWordCount(int newWordCount);
        void applyTheme(const Theme& theme);
        void openHtmlPreview();
        void openRecentFile();
        void refreshRecentFiles();
        void clearRecentFileHistory();
        void changeDocumentDisplayName(const QString& displayName);
        void onOperationStarted(const QString& description);
        void onOperationFinished();
        void changeFont();
        void onSetDictionary();
        void onSetLocale();
        void showAutoMatchFilterDialog();

	private:
        QHBoxLayout* horizontalLayout_;
        QVBoxLayout* verticalLayout1_;
        QVBoxLayout* verticalLayout2_;
        QVBoxLayout* verticalLayout3_;
        MarkdownEditor* editor_;
        MarkdownHighlighter* highlighter_;
        DocumentManager* documentManager_;
        ThemeFactory* themeFactory_;
        Theme theme_;
        QString language_;
        QLabel* wordCountLabel_;
        FindDialog* findReplaceDialog_;
        HtmlPreview* htmlPreviewWidget_;
        QWebView* quickReferenceGuideViewer_;
        QAction* fullScreenMenuAction_;
        QCheckBox* fullScreenButton_;
        QWidget* editorPane_;
        QFrame* centralStatusBar_;
        Outline* outlineWidget_;
        SpellBook* spellBookWidget_;
        QImage originalBackgroundImage_;
        QImage adjustedBackgroundImage_;
        QFileSystemWatcher* fileWatcher_;
        QLabel* statusLabel_;
        QAction* recentFilesActions_[MAX_RECENT_FILES];
        EffectsMenuBar* effectsMenuBar_;

        Exporter* exporter_;
        QThread* exporterThread_;

        AppSettings* appSettings_;

        QAction* addMenuAction(QMenu* menu, const QString& name, const QString& shortcut = 0,
                               bool checkable = false, bool checked = false,
                               QActionGroup* actionGroup = 0);

        void buildMenuBar();
        void buildStatusBar();

        void applyTheme();
        void predrawBackgroundImage();
};

#endif
