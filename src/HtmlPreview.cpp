/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014-2016 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include <QVariant>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStatusBar>
#include <QList>
#include <QPushButton>
#include <QPrintPreviewDialog>
#include <QApplication>
#include <QClipboard>
#include <QStack>
#include <QDir>
#include <QDesktopServices>
#include <QAction>
#include <QtConcurrentRun>
#include <QFuture>
#include <QSettings>
#include <QPrinter>
#include <QDesktopWidget>

#include "MainWindow.h"
#include "HtmlPreview.h"
#include "ColorHelper.h"
#include "Exporter.h"
#include "ExporterFactory.h"
#include "ExportDialog.h"
#include "MessageBoxHelper.h"
#include "StyleSheetManagerDialog.h"
#include "Theme.h"

#define GW_CUSTOM_STYLE_SHEETS_KEY "Preview/customStyleSheets"
#define GW_LAST_USED_STYLE_SHEET_KEY "Preview/lastUsedStyleSheet"
#define GW_LAST_USED_EXPORTER_KEY "Preview/lastUsedExporter"


HtmlPreview::HtmlPreview(TextDocument* document, QWidget *parent) : QWidget(parent),
    document_{document},
    handlingStyleSheetChange_{false}
{
    QSettings settings{};
    QString currentCssFile{settings.value(GW_LAST_USED_STYLE_SHEET_KEY, QString()).toString()};
    QString currentExporterName{settings.value(GW_LAST_USED_EXPORTER_KEY).toString()};
    customCssFiles_ = settings.value(GW_CUSTOM_STYLE_SHEETS_KEY, QStringList()).toStringList();

    QGridLayout* layout{new QGridLayout(this)};

    htmlBrowser_ = new QWebView();

    layout->addWidget(htmlBrowser_);
    htmlBrowser_->settings()->setDefaultTextEncoding("utf-8");

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    html_ = "";
    htmlBrowser_->setHtml("");
    htmlBrowser_->page()->setContentEditable(false);
    htmlBrowser_->page()->setLinkDelegationPolicy(QWebPage::DelegateExternalLinks);
    htmlBrowser_->page()->action(QWebPage::Reload)->setVisible(false);
    htmlBrowser_->page()->action(QWebPage::OpenLink)->setVisible(false);
    htmlBrowser_->page()->action(QWebPage::OpenLinkInNewWindow)->setVisible(false);
    connect(htmlBrowser_, SIGNAL(linkClicked(QUrl)), this, SLOT(onLinkClicked(QUrl)));
    headingTagExp_.setMinimal(true);
    headingTagExp_.setPattern("[Hh][1-6]");

    statusBar_ = new QStatusBar{};

    layout->addWidget(statusBar_);

    statusBar_->setSizeGripEnabled(false);
    statusBar_->setFixedHeight(32);

    updateStyle();

    defaultStyleSheets_.append(":/resources/github.css");
    defaultStyleSheets_.append(":/resources/diaspora.css");
    defaultStyleSheets_.append(":/resources/loomio.css");

    QPushButton* copyHtmlButton{new QPushButton{tr("Copy HTML")}};
    copyHtmlButton->setFocusPolicy(Qt::NoFocus);
    connect(copyHtmlButton, SIGNAL(clicked()), this, SLOT(copyHtml()));
    statusBar_->addPermanentWidget(copyHtmlButton);

    QPushButton* exportButton{new QPushButton{tr("Export")}};
    exportButton->setFocusPolicy(Qt::NoFocus);
    connect(exportButton, SIGNAL(clicked()), this, SLOT(onExport()));
    statusBar_->addPermanentWidget(exportButton);

    QPushButton* printButton{new QPushButton{tr("Print")}};
    printButton->setFocusPolicy(Qt::NoFocus);
    connect(printButton, SIGNAL(clicked()), this, SLOT(printPreview()));
    statusBar_->addPermanentWidget(printButton);

    QList<Exporter*> exporters{ExporterFactory::getInstance()->getHtmlExporters()};

    if (exporters.isEmpty()) {
        setHtml(QString("<b style='color: red'>") +
            tr("No markdown (pandoc, multimarkdown, discount) processors are "
                "installed.  Please install or add their installation locations "
                "to your system PATH environment variable.") + QString("</b>"));
        exporter_ = NULL;
    } else {
        int currentExporterIndex{0};

        for (int i{0}; i < exporters.length(); ++i) {
            Exporter* exporter{exporters.at(i)};

            if (exporter->getName() == currentExporterName)
                currentExporterIndex = i;
        }

        exporter_ = exporters.at(currentExporterIndex);
    }

    styleSheetComboBox_ = new QComboBox{this};
    styleSheetComboBox_->addItem(tr("GitHub"));
    styleSheetComboBox_->setItemData(0, QVariant(defaultStyleSheets_.at(0)));
    styleSheetComboBox_->addItem(tr("diaspora*"));
    styleSheetComboBox_->setItemData(1, QVariant(defaultStyleSheets_.at(1)));
    styleSheetComboBox_->addItem(tr("Loomio"));
    styleSheetComboBox_->setItemData(2, QVariant(defaultStyleSheets_.at(2)));
    styleSheetComboBox_->setMinimumWidth(128);
    styleSheetComboBox_->setMaximumWidth(256);
    styleSheetComboBox_->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

    int customCssIndexStart{defaultStyleSheets_.size()};

    int cssIndex{0};
    lastStyleSheetIndex_ = -1;

    for (int i{0}; i < customCssFiles_.size(); ++i) {
        QFileInfo fileInfo{customCssFiles_.at(i)};

        if (fileInfo.exists()) {
            styleSheetComboBox_->addItem(fileInfo.completeBaseName());
            styleSheetComboBox_->setItemData(customCssIndexStart + i, QVariant(customCssFiles_.at(i)));
        }
    }

    styleSheetComboBox_->addItem(tr("Add/Remove Custom Style Sheets..."));

    // Find the last used style sheet, and set it as selected in the combo box.
    for (int i{0}; i < styleSheetComboBox_->count() - 1; ++i) {
        if (styleSheetComboBox_->itemData(i).toString() == currentCssFile) {
            cssIndex = i;
            styleSheetComboBox_->setCurrentIndex(i);
            break;
        }
    }

    connect(styleSheetComboBox_, SIGNAL(currentIndexChanged(int)), this, SLOT(changeStyleSheet(int)));
    statusBar_->addWidget(styleSheetComboBox_);

    futureWatcher_ = new QFutureWatcher<QString>{this};
    this->connect(futureWatcher_, SIGNAL(finished()), SLOT(onHtmlReady()));
    this->changeStyleSheet(cssIndex);

    this->connect(document, SIGNAL(filePathChanged()), SLOT(updateBaseDir()));
    this->updateBaseDir();

    // Set up default page layout and page size for printing.
    printer_.setPaperSize(QPrinter::Letter);
    printer_.setPageMargins(0.5, 0.5, 0.5, 0.5, QPrinter::Inch);

    // Set zoom factor for WebKit browser to account for system DPI settings,
    // since WebKit assumes 96 DPI as a fixed resolution.
    //
    QWidget* window{QApplication::desktop()->screen()};
    int horizontalDpi{window->logicalDpiX()};
    // Don't want to affect image size, only text size.
    htmlBrowser_->settings()->setAttribute(QWebSettings::ZoomTextOnly, true);
    htmlBrowser_->setZoomFactor((horizontalDpi / 96.0));
}

HtmlPreview::~HtmlPreview()
{
    QSettings settings{};

    // Store the selected exporter name.
    QString exporterName{};

    if (!exporterName.isNull()) {
        settings.setValue(GW_LAST_USED_EXPORTER_KEY, exporterName);
    } else {
        // Clean up key if no exporters were even installed from which
        // to select.
        //
        settings.remove(GW_LAST_USED_EXPORTER_KEY);
    }

    // Store the custom style sheet list.
    settings.setValue(GW_CUSTOM_STYLE_SHEETS_KEY, customCssFiles_);

    // Store the last used style sheet.
    // Use count > 1 to exclude the "Add/Remove Custom Style Sheets" option.
    if ((styleSheetComboBox_->count() > 1) && (styleSheetComboBox_->currentIndex() < (styleSheetComboBox_->count() - 1))) {
        QString cssPath{styleSheetComboBox_->itemData(styleSheetComboBox_->currentIndex()).toString()};
        settings.setValue(GW_LAST_USED_STYLE_SHEET_KEY, cssPath);
    }

    // Wait for thread to finish if in the middle of updating the preview.
    futureWatcher_->waitForFinished();
}

void HtmlPreview::updateStyle()
{
    QString styleSheet{};
    QTextStream stream{&styleSheet};

    Theme theme = static_cast<MainWindow*>(parentWidget())->getTheme();

    stream << "QStatusBar { margin: 0; padding: 0; background-color:"
           << ColorHelper::toRgbString(theme.getEditorBackgroundColor())
           << "} "
           << "QStatusBar::item { border: 0; margin: 0; padding: 0; border: 0px } "
           << "QPushButton { font-weight: bold; margin: 0 1px 0 1px; padding: 5px; "
           << "border-radius: 5px; background: #66A15D; color: #ffffaa } "
           << "QPushButton:hover { background: #42ee23 } "
           << "QPushButton:pressed, QPushButton:flat, QPushButton:checked "
           << "{ background-color: #66ffaa }";

    statusBar_->setStyleSheet(styleSheet);
}

QString HtmlPreview::getStyleSheet()
{

    QString filePath{styleSheetComboBox_->itemData(styleSheetComboBox_->currentIndex()).toString()};
    QFile style{filePath};
    if (!style.open(QFile::ReadOnly | QFile::Text)) return "";
    QTextStream in(&style);
    return in.readAll();
}

QString HtmlPreview::getStyleSheetName() const
{
    return styleSheetComboBox_->currentText();
}

void HtmlPreview::updatePreview()
{
    if (this->isVisible()) {
        // Some markdown processors don't handle empty text very well
        // and will error.  Thus, only pass in text from the document
        // into the markdown processor if the text isn't empty or null.
        //
        if (document_->isEmpty()) {
            this->setHtml("");
        } else if (NULL != exporter_) {
            QString text{document_->toPlainText()};

            if (!text.isNull() && !text.isEmpty()) {
                QFuture<QString> future{QtConcurrent::run(
                        this, &HtmlPreview::exportToHtml, document_->toPlainText(), exporter_)};

                futureWatcher_->setFuture(future);
            }
        }
    }
}

void HtmlPreview::navigateToHeading(int headingSequenceNumber)
{
    QString anchor{QString("livepreviewhnbr%1").arg(headingSequenceNumber)};
    htmlBrowser_->page()->mainFrame()->scrollToAnchor(anchor);
}

void HtmlPreview::onHtmlReady()
{
    QString html{futureWatcher_->result()};

    // Don't bother updating if the HTML didn't change.
    if (html == this->html_)
        return;

    // Find where the change occurred since last time, and slip an
    // anchor in the location so that we can scroll there.
    //
    QString anchoredHtml{""};

    QTextStream newHtmlDoc{static_cast<QString*>(&html), QIODevice::ReadOnly};
    QTextStream oldHtmlDoc{static_cast<QString*>(&(this->html_)), QIODevice::ReadOnly};
    QTextStream anchoredHtmlDoc{&anchoredHtml, QIODevice::WriteOnly};

    bool differenceFound{false};
    QString oldLine{oldHtmlDoc.readLine()};
    QString newLine{newHtmlDoc.readLine()};

    while (!oldLine.isNull() && !newLine.isNull() && !differenceFound) {
        if (oldLine != newLine) {
            // Found the difference, so insert an anchor point at the
            // beginning of the line.
            //
            differenceFound = true;
            anchoredHtmlDoc << "<div id=\"livepreviewmodifypoint\" />";
        } else {
            anchoredHtmlDoc << newLine << "\n";
            oldLine = oldHtmlDoc.readLine();
            newLine = newHtmlDoc.readLine();
        }
    }

    // Put any remaining new HTML data into the
    // anchored HTML string.
    //
    while (!newLine.isNull()) {
        anchoredHtmlDoc << newLine << "\n";
        newLine = newHtmlDoc.readLine();
    }

    setHtml(anchoredHtml);
    this->html_ = html;

    // Traverse the DOM in the browser, and find all the H1-H6 tags.
    // Set the id attribute of each heading tag to have a unique
    // sequence number, so that when the navigateToHeading() slot
    // is triggered, we can scroll to the desired heading.
    //
    QWebFrame* frame{htmlBrowser_->page()->mainFrame()};
    QWebElement element{frame->documentElement()};
    QStack<QWebElement> elementStack{};
    int headingId{1};

    elementStack.push(element);

    while (!elementStack.isEmpty()) {
        element = elementStack.pop();

        // If the element is a heading tag (H1-H6), set an anchor id for it.
        if (headingTagExp_.exactMatch(element.tagName())) {
            element.prependOutside(QString("<span id='livepreviewhnbr%1'></span>").arg(headingId));
            headingId++;
        // else if the element is something that would have a heading tag
        // (not a paragraph, blockquote, code, etc.), then add its children
        // to traverse and look for headings.
        //
        } else if ((0 != element.tagName().compare("blockquote", Qt::CaseInsensitive))
            && (0 != element.tagName().compare("code", Qt::CaseInsensitive))
            && (0 != element.tagName().compare("p", Qt::CaseInsensitive))
            && (0 != element.tagName().compare("ol", Qt::CaseInsensitive))
            && (0 != element.tagName().compare("ul", Qt::CaseInsensitive))
            && (0 != element.tagName().compare("table", Qt::CaseInsensitive))) {
            QStack<QWebElement> childStack{};
            element = element.firstChild();

            while (!element.isNull()) {
                childStack.push(element);
                element = element.nextSibling();
            }

            while (!childStack.isEmpty()) {
                elementStack.push(childStack.pop());
            }
        }
    }
}

void HtmlPreview::changeStyleSheet(int index)
{
    // Prevent recursion, since calls to the combo box's setCurrentIndex
    // method will trigger changeStyleSheet to be called again.
    //
    if (handlingStyleSheetChange_)
        return;

    bool previewUpdateNeeded{true};

    handlingStyleSheetChange_ = true;

    QString filePath{};
    int selectionIndex{index};

    // If the "Add/Remove Custom Style Sheets" option was selected...
    if (styleSheetComboBox_->count() == (index + 1)) {
        // Save off the style sheet file path of the last selected item.
        QString oldSelection{styleSheetComboBox_->itemData(lastStyleSheetIndex_).toString()};

        // Now make sure the last style sheet is what is selected in the
        // combo box, that way the user doesn't see the
        // "Add/Remove Custom Style Sheets" as selected.
        //
        styleSheetComboBox_->setCurrentIndex(lastStyleSheetIndex_);

        // Let the user add/remove style sheets via the StyleSheetManagerDialog.
        StyleSheetManagerDialog ssmDialog{customCssFiles_, this};
        int result{ssmDialog.exec()};

        // If changes are accepted (user clicked OK), reload the style sheets
        // into the combo box, and select the last used style sheet if it
        // wasn't removed.
        //
        if (QDialog::Accepted == result) {
            customCssFiles_ = ssmDialog.getStyleSheets();

            // Remove all the old style sheets from the combo box.
            while (styleSheetComboBox_->count() > (defaultStyleSheets_.size() + 1)) {
                styleSheetComboBox_->removeItem(1);
            }

            selectionIndex = 0;

            // Now put the new list of style sheets back into the combo box.
            for (int i{customCssFiles_.size() - 1}; i >= 0; --i) {
                QString styleSheet{customCssFiles_[i]};
                QFileInfo fileInfo{styleSheet};
                styleSheetComboBox_->insertItem(defaultStyleSheets_.size(), fileInfo.completeBaseName(),
                    styleSheet);

                if (styleSheet == oldSelection) {
                    previewUpdateNeeded = false;
                    selectionIndex = i + 1;
                }
            }

            // If the last selected style sheet was one of the default ones,
            // and is still currently selected, then we don't need to update
            // the preview again.
            //
            if ((lastStyleSheetIndex_ == selectionIndex) && (selectionIndex < defaultStyleSheets_.size()))
                previewUpdateNeeded = false;

            styleSheetComboBox_->setCurrentIndex(selectionIndex);
            filePath = styleSheetComboBox_->itemData(selectionIndex).toString();
        } else {
            // If the user canceled adding/removing custom CSS files, return.
            handlingStyleSheetChange_ = false;
            return;
        }
    } else {
        filePath = styleSheetComboBox_->itemData(index).toString();
    }

    // Update the HTML preview with the newly selected style sheet, if needed.
    if (previewUpdateNeeded) {
        if (selectionIndex >= defaultStyleSheets_.size()) {
            htmlBrowser_->settings()->setUserStyleSheetUrl(QUrl::fromLocalFile(filePath));
        } else {
            htmlBrowser_->settings()->setUserStyleSheetUrl(
                QUrl(QString("qrc") + defaultStyleSheets_.at(selectionIndex)));
        }

        setHtml("");
        updatePreview();
    }

    lastStyleSheetIndex_ = selectionIndex;
    handlingStyleSheetChange_ = false;
}

void HtmlPreview::printPreview()
{
    QPrintPreviewDialog printPreviewDialog{&printer_, this};

    connect(&printPreviewDialog, SIGNAL(paintRequested(QPrinter*)), this, SLOT(printHtmlToPrinter(QPrinter*)));

    printPreviewDialog.exec();
}

void HtmlPreview::printHtmlToPrinter(QPrinter* printer)
{
    this->htmlBrowser_->print(printer);
}

void HtmlPreview::onExport()
{
    ExportDialog exportDialog{document_, this};

    connect(&exportDialog, SIGNAL(exportStarted(QString)), this, SIGNAL(operationStarted(QString)));
    connect(&exportDialog, SIGNAL(exportComplete()), this, SIGNAL(operationFinished()));

    exportDialog.exec();
}

void HtmlPreview::copyHtml()
{
    QClipboard *clipboard{QApplication::clipboard()};
    clipboard->setText(html_);
}

void HtmlPreview::onLinkClicked(const QUrl& url)
{
    QDesktopServices::openUrl(url);
}

void HtmlPreview::updateBaseDir()
{
    if (!document_->getFilePath().isNull() && !document_->getFilePath().isEmpty()) {
        // Note that a forward slash ("/") is appended to the path to
        // ensure it works.  If the slash isn't there, then it won't
        // recognize the base URL for some reason.
        //
        this->baseUrl_ =
            QUrl::fromLocalFile(QFileInfo(document_->getFilePath()).dir().absolutePath()
                + "/");
    } else {
        this->baseUrl_ = QUrl();
    }

    this->updatePreview();
}

QSize HtmlPreview::sizeHint() const
{
    return QSize(500, 600);
}

void HtmlPreview::closeEvent(QCloseEvent* event)
{
    Q_UNUSED(event);

    setHtml("");
    html_ = "";
}

void HtmlPreview::setHtml(const QString& html)
{
    QPoint previousScrollPosition = htmlBrowser_->page()->mainFrame()->scrollPosition();

    this->html_ = html;

    htmlBrowser_->setContent(html.toUtf8(), "text/html", baseUrl_);
    htmlBrowser_->page()->mainFrame()->setScrollPosition(previousScrollPosition);
}

QString HtmlPreview::exportToHtml(const QString& text, Exporter* exporter) const
{
    QString html{};
    exporter->exportToHtml(text, html);

    return html;
}
