/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014, 2015 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#ifndef SPELLBOOK_H
#define SPELLBOOK_H


#include <QListWidget>
#include <QString>

#include "MarkdownEditor.h"

/**
 * SpellBook widget for recalling shortkeys and applying formatting
 */
class SpellBook : public QListWidget
{
    Q_OBJECT
public:
    SpellBook(MarkdownEditor *editor, QWidget* parent = 0);
private:
    MarkdownEditor* editor_;
private slots:
    void onSpellSelected();
};

#endif // SPELLBOOK_H
