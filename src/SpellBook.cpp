/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014, 2015 wereturtle
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include <QListWidgetItem>
#include <QVariant>

#include "SpellBook.h"

SpellBook::SpellBook(MarkdownEditor* editor, QWidget *parent)
    : QListWidget(parent),
    editor_{editor}
{
    addItem(tr("# Heading 1"));
    addItem(tr("## Heading 2"));
    addItem(tr("### Heading 3"));
    addItem(tr("#### Heading 4"));
    addItem(tr("##### Heading 5"));
    addItem(tr("###### Heading 6"));
    addItem(tr("*Emphasis* _Emphasis_"));
    addItem(tr("**Strong** __Strong__"));
    addItem(tr("~~Strikethrough~~"));
    addItem(tr("[Link](http://url.com \"Title\")"));
    addItem(tr("![Image](./image.jpg \"Title\")"));
    addItem(tr("* Bullet List"));
    addItem(tr("- Bullet List"));
    addItem(tr("+ Bullet List"));
    addItem(tr("1. Numbered List"));
    addItem(tr("1) Numbered List"));
    addItem(tr("> Block Quote"));
    addItem(tr("`Code Span`"));
    addItem(tr("``` Code Block"));
    addItem(tr("--- *** ___ Horizontal Rule"));

    item(6)->setToolTip("Ctrl+I");
    item(7)->setToolTip("Ctrl+B");
    item(8)->setToolTip("Ctrl+K");

    item(11)->setToolTip("Ctrl+*");
    item(12)->setToolTip("Ctrl+-");
    item(13)->setToolTip("Ctrl++");
    item(14)->setToolTip("Ctrl+\\");
    item(15)->setToolTip("Ctrl+]");
    item(16)->setToolTip("Ctrl+.");
    item(17)->setToolTip("Ctrl+J");
    item(18)->setToolTip("Ctrl+U");
    item(19)->setToolTip("Ctrl+H");

    connect(this, SIGNAL(itemActivated(QListWidgetItem*)), this, SLOT(onSpellSelected()));
    connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onSpellSelected()));
}

void SpellBook::onSpellSelected()
{
    switch (currentRow()) {
    case 0: case 1: case 2: case 3: case 4: case 5:
        editor_->createHeaderWithHashMarker(currentRow()+1);
        break;
    case 6: editor_->italic();
        break;
    case 7: editor_->bold();
        break;
    case 8: editor_->strikethrough();
        break;
    case 9: //Link
        break;
    case 10: //Image
        break;
    case 11: editor_->createBulletListWithAsteriskMarker();
        break;
    case 12: editor_->createBulletListWithMinusMarker();
        break;
    case 13: editor_->createBulletListWithPlusMarker();
        break;
    case 14: editor_->createNumberedListWithPeriodMarker();
        break;
    case 15: editor_->createNumberedListWithParenthesisMarker();
        break;
    case 16: editor_->createBlockquote();
        break;
    case 17: editor_->createCodeSpan();
        break;
    case 18: editor_->createCodeBlock();
        break;
    case 19: editor_->insertRule();
        break;
    }
}

