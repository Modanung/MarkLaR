/***********************************************************************
 *
 * Copyright (C) 2016 Marklar
 * Copyright (C) 2014-2016 wereturtle
 * Copyright (C) 2009, 2010, 2011, 2012, 2013, 2014 Graeme Gott <graeme@gottcode.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include <QWidget>
#include <QApplication>
#include <QIcon>
#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QStatusBar>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextStream>
#include <QIODevice>
#include <QFile>
#include <QFontMetrics>
#include <QFont>
#include <QCommonStyle>
#include <QScrollBar>
#include <QGridLayout>
#include <QGraphicsColorizeEffect>
#include <QLocale>
#include <QPainter>
#include <QSettings>
#include <QFontDialog>
#include <QTextBrowser>
#include <QCheckBox>
#include <QInputDialog>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QDialogButtonBox>

#include "MainWindow.h"
#include "ThemeFactory.h"
#include "HtmlPreview.h"
#include "find_dialog.h"
#include "ColorHelper.h"
#include "ThemeSelectionDialog.h"
#include "MarkdownHighlighter.h"
#include "DocumentManager.h"
#include "DocumentHistory.h"
#include "Outline.h"
#include "SpellBook.h"
#include "MessageBoxHelper.h"
#include "SimpleFontDialog.h"
#include "LocaleDialog.h"

#define GW_MAIN_WINDOW_GEOMETRY_KEY "Window/mainWindowGeometry"
#define GW_MAIN_WINDOW_STATE_KEY "Window/mainWindowState"
#define GW_HTML_PREVIEW_GEOMETRY_KEY "Preview/htmlPreviewGeometry"
#define GW_HTML_PREVIEW_OPEN "Preview/htmlPreviewOpen"
#define GW_SPELLBOOK_OPEN_KEY "Preview/htmlPreviewOpen"

void MainWindow::showEditor()
{
    if (!editorPane_->isVisible()){
        editorPane_->show();
        centralStatusBar_->show();
    } else {
        editorPane_->hide();
        centralStatusBar_->hide();
    }
}

MainWindow::MainWindow(const QString& filePath, QWidget* parent)
    : QMainWindow(parent),
    horizontalLayout_{new QHBoxLayout{}},
    verticalLayout1_{new QVBoxLayout{}},
    verticalLayout2_{new QVBoxLayout{}},
    verticalLayout3_{new QVBoxLayout{}}
{
    QString fileToOpen{};
    this->setWindowIcon(QIcon{":/resources/images/marklar.svg"});
    this->setObjectName("mainWindow");
    this->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    this->statusBar()->setMaximumHeight(0);
    this->statusBar()->setEnabled(false);
    this->statusBar()->setSizeGripEnabled(false);

    QWidget* centralWidget{new QWidget{}};

    verticalLayout1_->setSizeConstraint(QLayout::SetDefaultConstraint);
    verticalLayout1_->setSpacing(0);
    verticalLayout1_->setMargin(0);
    verticalLayout2_->setSpacing(0);
    verticalLayout2_->setMargin(0);
    verticalLayout3_->setSpacing(0);
    verticalLayout3_->setMargin(0);
    horizontalLayout_->setSpacing(0);
    horizontalLayout_->setMargin(0);

    horizontalLayout_->addLayout(verticalLayout1_);
    horizontalLayout_->addLayout(verticalLayout2_);
    horizontalLayout_->addLayout(verticalLayout3_);

    centralWidget->setLayout(horizontalLayout_);
    setCentralWidget(centralWidget);

    appSettings_ = AppSettings::getInstance();

    // We need to set an empty style for the editor's scrollbar in order for the
    // scrollbar CSS stylesheet to take full effect.  Otherwise, the scrollbar's
    // background color will have the Windows 98 checkered look rather than
    // being a solid or transparent color.
    //
    outlineWidget_ = new Outline{this};

    outlineWidget_->setMinimumHeight(64);
    outlineWidget_->setMaximumHeight(16777215);
    outlineWidget_->verticalScrollBar()->setStyle(new QCommonStyle{});
    outlineWidget_->horizontalScrollBar()->setStyle(new QCommonStyle{});
    outlineWidget_->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

    verticalLayout1_->addWidget(outlineWidget_);

    // Set empty style so that stylesheet takes full effect. (See comment
    // above on outlineWidget for more details.)
    //


    TextDocument* document{new TextDocument{}};

    // The below connection must happen before the Highlighter is created and
    // connected to the text document. See note in Outline class's
    // onTextChanged() slot.
    //
    connect(document, SIGNAL(contentsChange(int,int,int)), outlineWidget_, SLOT(onTextChanged(int,int,int)));

    highlighter_ = new MarkdownHighlighter{document};
    highlighter_->setSpellCheckEnabled(appSettings_->getLiveSpellCheckEnabled());
    connect(highlighter_, SIGNAL(headingFound(int,int,QString)), outlineWidget_, SLOT(insertHeadingIntoOutline(int,int,QString)));
    connect(highlighter_, SIGNAL(headingRemoved(int)), outlineWidget_, SLOT(removeHeadingFromOutline(int)));

    editor_ = new MarkdownEditor{document, highlighter_};
    editor_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    editor_->setFont(appSettings_->getFont().family(), appSettings_->getFont().pointSize());
    editor_->setAutoMatchEnabled(appSettings_->getAutoMatchEnabled());
    editor_->setBulletPointCyclingEnabled(appSettings_->getBulletPointCyclingEnabled());
    editor_->setPlainText("");
    connect(outlineWidget_, SIGNAL(documentPositionNavigated(int)), editor_, SLOT(navigateDocument(int)));
    connect(editor_, SIGNAL(cursorPositionChanged(int)), outlineWidget_, SLOT(updateCurrentNavigationHeading(int)));

    spellBookWidget_ = new SpellBook{editor_, this};

    spellBookWidget_->setBaseSize(QSize(128, 416));
    spellBookWidget_->setMinimumHeight(64);
    spellBookWidget_->setMaximumHeight(464);
    spellBookWidget_->verticalScrollBar()->setStyle(new QCommonStyle{});
    spellBookWidget_->horizontalScrollBar()->setStyle(new QCommonStyle{});
    spellBookWidget_->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

    verticalLayout1_->addWidget(spellBookWidget_);

    // We need to set an empty style for the editor's scrollbar in order for the
    // scrollbar CSS stylesheet to take full effect.  Otherwise, the scrollbar's
    // background color will have the Windows 98 checkered look rather than
    // being a solid or transparent color.
    //
    editor_->verticalScrollBar()->setStyle(new QCommonStyle{});
    editor_->horizontalScrollBar()->setStyle(new QCommonStyle{});

    documentManager_ = new DocumentManager{editor_, htmlPreviewWidget_, this};
    documentManager_->setFileHistoryEnabled(appSettings_->getFileHistoryEnabled());
    setWindowTitle(documentManager_->getDocument()->getDisplayName() + "[*] - " + STYLIZED_NAME);
    connect(documentManager_, SIGNAL(documentDisplayNameChanged(QString)), this, SLOT(changeDocumentDisplayName(QString)));
    connect(documentManager_, SIGNAL(documentModifiedChanged(bool)), this, SLOT(setWindowModified(bool)));
    connect(documentManager_, SIGNAL(operationStarted(QString)), this, SLOT(onOperationStarted(QString)));
    connect(documentManager_, SIGNAL(operationFinished()), this, SLOT(onOperationFinished()));
    connect(documentManager_, SIGNAL(documentClosed()), this, SLOT(refreshRecentFiles()));

    editor_->setAutoMatchEnabled('\"', appSettings_->getAutoMatchDoubleQuotes());
    editor_->setAutoMatchEnabled('\'', appSettings_->getAutoMatchSingleQuotes());
    editor_->setAutoMatchEnabled('(', appSettings_->getAutoMatchParentheses());
    editor_->setAutoMatchEnabled('[', appSettings_->getAutoMatchSquareBrackets());
    editor_->setAutoMatchEnabled('{', appSettings_->getAutoMatchBraces());
    editor_->setAutoMatchEnabled('*', appSettings_->getAutoMatchAsterisks());
    editor_->setAutoMatchEnabled('_', appSettings_->getAutoMatchUnderscores());
    editor_->setAutoMatchEnabled('`', appSettings_->getAutoMatchBackticks());
    editor_->setAutoMatchEnabled('<', appSettings_->getAutoMatchAngleBrackets());

    editorPane_ = new QWidget{this};
    editorPane_->setObjectName("editorLayoutArea");
    editorPane_->setLayout(editor_->getPreferredLayout());
    editorPane_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    verticalLayout2_->addWidget(editorPane_);

    findReplaceDialog_ = new FindDialog{editor_};
    findReplaceDialog_->setModal(false);
    connect(findReplaceDialog_, SIGNAL(replaceAllComplete()), editor_, SLOT(refreshWordCount()));

    QStringList recentFiles{};

    if (appSettings_->getFileHistoryEnabled()) {
        DocumentHistory history{};
        recentFiles = history.getRecentFiles(MAX_RECENT_FILES + 2);
    }

    if (!filePath.isNull() && !filePath.isEmpty()) {
        QFileInfo cliFileInfo{filePath};

        if (cliFileInfo.exists()) {
            fileToOpen = filePath;
            recentFiles.removeAll(cliFileInfo.absoluteFilePath());
        } else {
            qCritical("File specified on command line does not exist.");
            exit(-1);
        }
    }

    if (fileToOpen.isNull() && appSettings_->getFileHistoryEnabled()) {
        QString lastFile{};

        if (!recentFiles.isEmpty())
            lastFile = recentFiles.first();

        if (QFileInfo(lastFile).exists()) {
            fileToOpen = lastFile;
            recentFiles.removeAll(lastFile);
        }
    }

    for (int i{0}; i < MAX_RECENT_FILES; ++i)
    {
        recentFilesActions_[i] = new QAction{this};
        connect(recentFilesActions_[i], SIGNAL(triggered()), this, SLOT(openRecentFile()));

        if (i < recentFiles.size()) {
            recentFilesActions_[i]->setText(recentFiles.at(i));
            recentFilesActions_[i]->setVisible(true);
            recentFilesActions_[i]->setShortcut(QKeySequence("Ctrl+" + QString::number(i+1)));
        } else {
            recentFilesActions_[i]->setVisible(false);
        }
    }

    htmlPreviewWidget_ = new HtmlPreview{documentManager_->getDocument(), this};

    connect(editor_, SIGNAL(typingPaused()), htmlPreviewWidget_, SLOT(updatePreview()));
    connect(outlineWidget_, SIGNAL(headingNumberNavigated(int)), htmlPreviewWidget_, SLOT(navigateToHeading(int)));
    connect(htmlPreviewWidget_, SIGNAL(operationStarted(QString)), this, SLOT(onOperationStarted(QString)));
    connect(htmlPreviewWidget_, SIGNAL(operationFinished()), this, SLOT(onOperationFinished()));

    verticalLayout3_->addWidget(htmlPreviewWidget_);

    buildMenuBar();
    buildStatusBar();

    QString themeName{appSettings_->getThemeName()};

    QString err{};

    theme_ = ThemeFactory::getInstance()->loadTheme(themeName, err);

    // Determine locale for dictionary language (for use in spell checking).
    language_ = DictionaryManager::instance().availableDictionary(appSettings_->getDictionaryLanguage());

    // If we have an available dictionary, then get it and set up spell checking.
    if (!language_.isNull() && !language_.isEmpty()) {
        DictionaryManager::instance().setDefaultLanguage(language_);
        DictionaryRef dictionary{DictionaryManager::instance().requestDictionary()};
        editor_->setDictionary(dictionary);
        editor_->setSpellCheckEnabled(appSettings_->getLiveSpellCheckEnabled());
    } else {
        editor_->setSpellCheckEnabled(false);
    }

    connect(editor_, SIGNAL(wordCountChanged(int)), this, SLOT(updateWordCount(int)));


    // Set dimensions for the window
    QSettings windowSettings{};

    if (windowSettings.contains(GW_MAIN_WINDOW_GEOMETRY_KEY)) {
        restoreGeometry(windowSettings.value(GW_MAIN_WINDOW_GEOMETRY_KEY).toByteArray());
        restoreState(windowSettings.value(GW_MAIN_WINDOW_STATE_KEY).toByteArray());

        if (this->isFullScreen())
            effectsMenuBar_->setAutoHideEnabled(appSettings_->getHideMenuBarInFullScreenEnabled());
        else
            effectsMenuBar_->setAutoHideEnabled(false);
    } else {
        this->adjustSize();
    }

    if (windowSettings.contains(GW_HTML_PREVIEW_GEOMETRY_KEY))
        htmlPreviewWidget_->restoreGeometry(windowSettings.value(GW_HTML_PREVIEW_GEOMETRY_KEY).toByteArray());
    else
        htmlPreviewWidget_->adjustSize();

    quickReferenceGuideViewer_ = NULL;

    show();

    if (windowSettings.value(GW_SPELLBOOK_OPEN_KEY, QVariant(false)).toBool())
        spellBookWidget_->show();

    if (windowSettings.value(GW_HTML_PREVIEW_OPEN, QVariant(false)).toBool())
        htmlPreviewWidget_->show();

    // Apply the theme only after show() is called on all the widgets,
    // since the Outline scrollbars can end up transparent in Windows if
    // the theme is applied before show().
    //
    applyTheme();

    if (!fileToOpen.isNull() && !fileToOpen.isEmpty())
        documentManager_->open(fileToOpen);

}

MainWindow::~MainWindow()
{
    if (NULL != htmlPreviewWidget_)
        delete htmlPreviewWidget_;

    if (NULL != quickReferenceGuideViewer_)
        delete quickReferenceGuideViewer_;
}

QSize MainWindow::sizeHint() const
{
    return QSize{800, 500};
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
    // Resize the editor's margins based on the new size of the window.
    editor_->setupPaperMargins(event->size().width());

    if (!originalBackgroundImage_.isNull())
        predrawBackgroundImage();
}

void MainWindow::keyPressEvent(QKeyEvent* e)
{
    int key{e->key()};

    switch (key) {
    case Qt::Key_Escape:
        if (this->isFullScreen())
            toggleFullscreen(false);
        break;
    default:
        break;
    }

    QMainWindow::keyPressEvent(e);
}

void MainWindow::paintEvent(QPaintEvent* event)
{
    QPainter painter{this};
    painter.fillRect(this->rect(), theme_.getBackgroundColor().rgb());

    if (!adjustedBackgroundImage_.isNull())
        painter.drawImage(0, 0, adjustedBackgroundImage_);

    if (EditorAspectStretch == theme_.getEditorAspect())
        painter.fillRect(this->rect(), theme_.getEditorBackgroundColor());

    painter.end();
    QMainWindow::paintEvent(event);
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    if (documentManager_->close())
        this->quitApplication();
    else
        event->ignore();
}

void MainWindow::quitApplication()
{
    if (documentManager_->close()) {
        appSettings_->store();

        QSettings windowSettings{};
        windowSettings.setValue(GW_MAIN_WINDOW_GEOMETRY_KEY, saveGeometry());
        windowSettings.setValue(GW_MAIN_WINDOW_STATE_KEY, saveState());
        windowSettings.setValue(GW_SPELLBOOK_OPEN_KEY, QVariant(spellBookWidget_->isVisible()));
        windowSettings.setValue(GW_HTML_PREVIEW_GEOMETRY_KEY, htmlPreviewWidget_->saveGeometry());
        windowSettings.setValue(GW_HTML_PREVIEW_OPEN, QVariant(htmlPreviewWidget_->isVisible()));
        windowSettings.sync();

        DictionaryManager::instance().addProviders();
        DictionaryManager::instance().setDefaultLanguage(language_);

		qApp->quit();
    }
}

void MainWindow::changeTheme()
{
    ThemeSelectionDialog* themeDialog{new ThemeSelectionDialog(theme_.getName(), this)};
    connect(themeDialog, SIGNAL(applyTheme(Theme)), this, SLOT(applyTheme(Theme)));
    themeDialog->show();
}


void MainWindow::showFindReplaceDialog()
{
    findReplaceDialog_->show();
}

void MainWindow::toggleFullscreen(bool checked)
{
    // This method can be called either from the menu bar (View->Full Screen)
    // or from the full screen toggle button on the status bar.  To keep their
    // "checked/unchecked" states in sync, we have to determine who called
    // this method (which "sender" triggered the event--the button or the
    // menu option), and set the other's checked/unchecked state accordingly.
    // Unfortunately, setting their checked states causes this slot to be
    // triggered again recursively.  This means we have to use only one of the
    // "senders" as the source of "truth" to determine whether to go
    // full screen or to restore.  Thus, whenever the sender is the button in
    // the status bar, we'll toggle full screen mode. Otherwise, we'll just
    // set the button's state, knowing that this slot will be triggered again
    // recursively in doing so and that the full screen mode will be toggled
    // accordingly.
    //

    if (this->isFullScreen() || !checked) {
        fullScreenMenuAction_->setChecked(false);
        showNormal();

        if (appSettings_->getHideMenuBarInFullScreenEnabled())
            effectsMenuBar_->setAutoHideEnabled(false);
    } else {
        fullScreenMenuAction_->setChecked(true);
        showFullScreen();

        if (appSettings_->getHideMenuBarInFullScreenEnabled())
            effectsMenuBar_->setAutoHideEnabled(true);
    }
}

void MainWindow::toggleHideMenuBarInFullScreen(bool checked)
{
    appSettings_->setHideMenuBarInFullScreenEnabled(checked);

    if (this->isFullScreen())
        effectsMenuBar_->setAutoHideEnabled(checked);
}

void MainWindow::toggleLiveSpellCheck(bool checked)
{
    appSettings_->setLiveSpellCheckEnabled(checked);
    editor_->setSpellCheckEnabled(checked);
}

void MainWindow::toggleFileHistoryEnabled(bool checked)
{
    if (!checked)
        this->clearRecentFileHistory();

    appSettings_->setFileHistoryEnabled(checked);
    documentManager_->setFileHistoryEnabled(checked);
}

void MainWindow::toggleAutoMatch(bool checked)
{
    editor_->setAutoMatchEnabled(checked);
    appSettings_->setAutoMatchEnabled(checked);
}

void MainWindow::toggleBulletPointCycling(bool checked)
{
    editor_->setBulletPointCyclingEnabled(checked);
    appSettings_->setBulletPointCyclingEnabled(checked);
}

void MainWindow::insertImage()
{
    QString startingDirectory{QString()};
    TextDocument* document{documentManager_->getDocument()};

    if (!document->isNew())
        startingDirectory = QFileInfo(document->getFilePath()).dir().path();

    QString imagePath {QFileDialog::getOpenFileName(this, tr("Insert Image"), startingDirectory,
                    QString("%1 (*.jpg *.jpeg *.gif *.png *.bmp);; %2").arg(tr("Images")).arg(tr("All Files")))};

    if (!imagePath.isNull() && !imagePath.isEmpty()) {
        QFileInfo imgInfo{imagePath};
        bool isRelativePath{false};

        if (imgInfo.exists()) {
            if (!document->isNew()) {
                QFileInfo docInfo{document->getFilePath()};

                if (docInfo.exists()) {
                    imagePath = docInfo.dir().relativeFilePath(imagePath);
                    isRelativePath = true;
                }
            }
        }

        if (!isRelativePath)
            imagePath = QString{"file://"} + imagePath;

        QTextCursor cursor{editor_->textCursor()};
        cursor.insertText(QString("![](%1)").arg(imagePath));
    }
}

void MainWindow::changeTabulationWidth()
{
    int width{QInputDialog::getInt(this, tr("Tabulation Width"), tr("Spaces"), appSettings_->getTabWidth(),
        appSettings_->MIN_TAB_WIDTH, appSettings_->MAX_TAB_WIDTH)};

    editor_->setTabStopWidth(width);
    appSettings_->setTabWidth(width);
}

void MainWindow::showQuickReferenceGuide()
{
    if (NULL == quickReferenceGuideViewer_) {
        QString filePath{QString(":/resources/quickreferenceguide_") + appSettings_->getLocale() + ".html"};

        if (!QFileInfo(filePath).exists()) {
            filePath = QString(":/resources/quickreferenceguide_") + appSettings_->getLocale().left(2) + ".html";

            if (!QFileInfo(filePath).exists())
                filePath = ":/resources/quickreferenceguide_en.html";
        }

        QFile inputFile{filePath};

        if (!inputFile.open(QIODevice::ReadOnly)) {
            MessageBoxHelper::critical(this, tr("Failed to open Quick Reference Guide."), inputFile.errorString());
            inputFile.close();
            return;
        }

        QTextStream inStream{&inputFile};
        inStream.setCodec("UTF-8");
        QString html{inStream.readAll()};
        inputFile.close();

        // Note that the parent widget for this new window must be NULL, so that
        // it will hide beneath other windows when it is deactivated.
        //
        quickReferenceGuideViewer_ = new QWebView{NULL};
        quickReferenceGuideViewer_->setWindowTitle(tr("Quick Reference Guide"));
        quickReferenceGuideViewer_->setWindowFlags(Qt::Window);
        quickReferenceGuideViewer_->settings()->setDefaultTextEncoding("utf-8");
        quickReferenceGuideViewer_->page()->setLinkDelegationPolicy(QWebPage::DelegateExternalLinks);
        quickReferenceGuideViewer_->page()->action(QWebPage::Reload)->setVisible(false);
        quickReferenceGuideViewer_->page()->action(QWebPage::OpenLink)->setVisible(false);
        quickReferenceGuideViewer_->page()->action(QWebPage::OpenLinkInNewWindow)->setVisible(false);
        quickReferenceGuideViewer_->settings()->setUserStyleSheetUrl(QUrl("qrc:/resources/github.css"));
        quickReferenceGuideViewer_->page()->setContentEditable(false);
        quickReferenceGuideViewer_->setContent(html.toUtf8(), "text/html");
        connect(quickReferenceGuideViewer_, SIGNAL(linkClicked(QUrl)), this, SLOT(onQuickRefGuideLinkClicked(QUrl)));

        // Set zoom factor for WebKit browser to account for system DPI settings,
        // since WebKit assumes 96 DPI as a fixed resolution.
        //
        QWidget* window{QApplication::desktop()->screen()};
        int horizontalDpi{window->logicalDpiX()};
        // Don't want to affect image size, only text size.
        quickReferenceGuideViewer_->settings()->setAttribute(QWebSettings::ZoomTextOnly, true);
        quickReferenceGuideViewer_->setZoomFactor((horizontalDpi / 96.0));

        quickReferenceGuideViewer_->resize(500, 600);
        quickReferenceGuideViewer_->adjustSize();
    }

    quickReferenceGuideViewer_->show();
    quickReferenceGuideViewer_->raise();
    quickReferenceGuideViewer_->activateWindow();
}

void MainWindow::showOutline()
{
    if (!outlineWidget_->isVisible())
        outlineWidget_->show();
    else
        outlineWidget_->hide();
}

void MainWindow::showSpellBook()
{
    if (!spellBookWidget_->isVisible()) {
        spellBookWidget_->show();
        spellBookWidget_->activateWindow();
    } else {
        spellBookWidget_->hide();
    }
}

void MainWindow::onQuickRefGuideLinkClicked(const QUrl& url)
{
    QDesktopServices::openUrl(url);
}

void MainWindow::showAbout()
{
    QString aboutText{QString("<p><b>") +  STYLIZED_NAME + QString(" ")
                + qApp->applicationVersion() + QString(" </b></p>")
                + tr("<p>Copyright &copy; 2016 Marklar</b>"
                     "<p>You may use and redistribute this software under the terms "
                     "of the<br><a href=\"http://www.gnu.org/licenses/gpl.html\">"
                     "GNU General Public License Version 3</a>.</p>"
                     "MarkLaR is a fork of <a href=\"https://github.com/wereturtle/ghostwriter/\">ghostwriter</a> "
                     "and was inspired by <a href=\"https://github.com/dvcrn/markright/\">MarkRight</a> ")};

    QMessageBox::about(this, tr("About %1").arg(STYLIZED_NAME), aboutText);
}

void MainWindow::updateWordCount(int newWordCount)
{
    wordCountLabel_->setText(tr("%n word(s)", "", newWordCount));
}

void MainWindow::applyTheme(const Theme& theme)
{
    theme_ = theme;
    applyTheme();
}

void MainWindow::openHtmlPreview()
{
    if (!htmlPreviewWidget_->isVisible()) {
        htmlPreviewWidget_->show();
        htmlPreviewWidget_->updatePreview();
    } else {
        htmlPreviewWidget_->hide();
    }
}

void MainWindow::openRecentFile()
{
    QAction* action{qobject_cast<QAction*>(this->sender())};

    if (NULL != action)
        documentManager_->open(action->text());
}

void MainWindow::refreshRecentFiles()
{
    if (appSettings_->getFileHistoryEnabled()) {
        DocumentHistory history;
        QStringList recentFiles = history.getRecentFiles(MAX_RECENT_FILES + 1);
        TextDocument* document = documentManager_->getDocument();

        if (!document->isNew()) {
            QString sanitizedPath{QFileInfo(document->getFilePath()).absoluteFilePath()};
            recentFiles.removeAll(sanitizedPath);
        }

        for (int i{0}; (i < MAX_RECENT_FILES) && (i < recentFiles.size()); ++i) {
            recentFilesActions_[i]->setText(recentFiles.at(i));
            recentFilesActions_[i]->setVisible(true);
        }

        for (int i{recentFiles.size()}; i < MAX_RECENT_FILES; ++i) {
            recentFilesActions_[i]->setVisible(false);
        }
    }
}

void MainWindow::clearRecentFileHistory()
{
    DocumentHistory history{};
    history.clear();

    for (int i{0}; i < MAX_RECENT_FILES; ++i) {
        recentFilesActions_[i]->setVisible(false);
    }
}

void MainWindow::changeDocumentDisplayName(const QString& displayName)
{
    setWindowTitle(displayName + QString("[*] - ") + STYLIZED_NAME);

    setWindowModified(false);
}

void MainWindow::onOperationStarted(const QString& description)
{
    statusLabel_->setText(description);
    statusLabel_->show();
    this->update();
    qApp->processEvents();
}

void MainWindow::onOperationFinished()
{
    statusLabel_->setText(QString());
    statusLabel_->hide();
    this->update();
    qApp->processEvents();
}

void MainWindow::changeFont()
{
    bool success{};

    QFont font{SimpleFontDialog::getFont(&success, editor_->font(), this)};

    if (success) {
        editor_->setFont(font.family(), font.pointSize());
        appSettings_->setFont(font);
    }
}

void MainWindow::onSetDictionary()
{
    DictionaryDialog dictionaryDialog{language_, this};
    int status{dictionaryDialog.exec()};

    if (QDialog::Accepted == status) {
        language_ = dictionaryDialog.getLanguage();
        DictionaryManager::instance().setDefaultLanguage(language_);
        editor_->setDictionary(DictionaryManager::instance().requestDictionary(language_));
        appSettings_->setDictionaryLanguage(language_);
    }
}

void MainWindow::onSetLocale()
{
    bool ok{};

    QString locale{LocaleDialog::getLocale(&ok, appSettings_->getLocale(), appSettings_->getTranslationsPath())};

    if (ok && (locale != appSettings_->getLocale())) {
        appSettings_->setLocale(locale);

        QMessageBox::information(this, QApplication::applicationName(),
                                 tr("Please restart the application for changes to take effect."));
    }
}

void MainWindow::showAutoMatchFilterDialog()
{
    QDialog autoMatchFilterDialog{this};
    autoMatchFilterDialog.setWindowTitle(tr("Matched Characters"));

    QVBoxLayout* layout{new QVBoxLayout()};

    QCheckBox* autoMatchDoubleQuotesCheckbox{new QCheckBox("\"")};
    autoMatchDoubleQuotesCheckbox->setCheckable(true);
    autoMatchDoubleQuotesCheckbox->setChecked(appSettings_->getAutoMatchDoubleQuotes());
    layout->addWidget(autoMatchDoubleQuotesCheckbox);

    QCheckBox* autoMatchSingleQuotesCheckbox{new QCheckBox("\'")};
    autoMatchSingleQuotesCheckbox->setCheckable(true);
    autoMatchSingleQuotesCheckbox->setChecked(appSettings_->getAutoMatchSingleQuotes());
    layout->addWidget(autoMatchSingleQuotesCheckbox);

    QCheckBox* autoMatchParenthesesCheckbox{new QCheckBox("( )")};
    autoMatchParenthesesCheckbox->setCheckable(true);
    autoMatchParenthesesCheckbox->setChecked(appSettings_->getAutoMatchParentheses());
    layout->addWidget(autoMatchParenthesesCheckbox);

    QCheckBox* autoMatchSquareBracketsCheckbox{new QCheckBox("[ ]")};
    autoMatchSquareBracketsCheckbox->setCheckable(true);
    autoMatchSquareBracketsCheckbox->setChecked(appSettings_->getAutoMatchSquareBrackets());
    layout->addWidget(autoMatchSquareBracketsCheckbox);

    QCheckBox* autoMatchBracesCheckbox{new QCheckBox("{ }")};
    autoMatchBracesCheckbox->setCheckable(true);
    autoMatchBracesCheckbox->setChecked(appSettings_->getAutoMatchBraces());
    layout->addWidget(autoMatchBracesCheckbox);

    QCheckBox* autoMatchAsterisksCheckbox{new QCheckBox("*")};
    autoMatchAsterisksCheckbox->setCheckable(true);
    autoMatchAsterisksCheckbox->setChecked(appSettings_->getAutoMatchAsterisks());
    layout->addWidget(autoMatchAsterisksCheckbox);

    QCheckBox* autoMatchUnderscoresCheckbox{new QCheckBox("_")};
    autoMatchUnderscoresCheckbox->setCheckable(true);
    autoMatchUnderscoresCheckbox->setChecked(appSettings_->getAutoMatchUnderscores());
    layout->addWidget(autoMatchUnderscoresCheckbox);

    QCheckBox* autoMatchBackticksCheckbox{new QCheckBox("`")};
    autoMatchBackticksCheckbox->setCheckable(true);
    autoMatchBackticksCheckbox->setChecked(appSettings_->getAutoMatchBackticks());
    layout->addWidget(autoMatchBackticksCheckbox);

    QCheckBox* autoMatchAngleBracketsCheckbox{new QCheckBox("< >")};
    autoMatchAngleBracketsCheckbox->setCheckable(true);
    autoMatchAngleBracketsCheckbox->setChecked(appSettings_->getAutoMatchAngleBrackets());
    layout->addWidget(autoMatchAngleBracketsCheckbox);

    autoMatchFilterDialog.setLayout(layout);

    QDialogButtonBox* buttonBox{new QDialogButtonBox(Qt::Horizontal, this)};
    buttonBox->addButton(QDialogButtonBox::Ok);
    buttonBox->addButton(QDialogButtonBox::Cancel);
    layout->addWidget(buttonBox);

    connect(buttonBox, SIGNAL(accepted()), &autoMatchFilterDialog, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), &autoMatchFilterDialog, SLOT(reject()));

    int status{autoMatchFilterDialog.exec()};

    if (QDialog::Accepted == status) {
        appSettings_->setAutoMatchDoubleQuotes(autoMatchDoubleQuotesCheckbox->isChecked());
        appSettings_->setAutoMatchSingleQuotes(autoMatchSingleQuotesCheckbox->isChecked());
        appSettings_->setAutoMatchParentheses(autoMatchParenthesesCheckbox->isChecked());
        appSettings_->setAutoMatchSquareBrackets(autoMatchSquareBracketsCheckbox->isChecked());
        appSettings_->setAutoMatchBraces(autoMatchBracesCheckbox->isChecked());
        appSettings_->setAutoMatchAsterisks(autoMatchAsterisksCheckbox->isChecked());
        appSettings_->setAutoMatchUnderscores(autoMatchUnderscoresCheckbox->isChecked());
        appSettings_->setAutoMatchBackticks(autoMatchBackticksCheckbox->isChecked());
        appSettings_->setAutoMatchAngleBrackets(autoMatchAngleBracketsCheckbox->isChecked());

        editor_->setAutoMatchEnabled('\"', appSettings_->getAutoMatchDoubleQuotes());
        editor_->setAutoMatchEnabled('\'', appSettings_->getAutoMatchSingleQuotes());
        editor_->setAutoMatchEnabled('(', appSettings_->getAutoMatchParentheses());
        editor_->setAutoMatchEnabled('[', appSettings_->getAutoMatchSquareBrackets());
        editor_->setAutoMatchEnabled('{', appSettings_->getAutoMatchBraces());
        editor_->setAutoMatchEnabled('*', appSettings_->getAutoMatchAsterisks());
        editor_->setAutoMatchEnabled('_', appSettings_->getAutoMatchUnderscores());
        editor_->setAutoMatchEnabled('`', appSettings_->getAutoMatchBackticks());
        editor_->setAutoMatchEnabled('<', appSettings_->getAutoMatchAngleBrackets());
    }
}

QAction* MainWindow::addMenuAction(QMenu* menu, const QString& name, const QString& shortcut,
    bool checkable, bool checked, QActionGroup* actionGroup)
{
    QAction* action{new QAction(name, this)};

    if (0 != shortcut)
        action->setShortcut(shortcut);

    action->setCheckable(checkable);
    action->setChecked(checked);

    if (0 != actionGroup)
        action->setActionGroup(actionGroup);

    menu->addAction(action);

    return action;
}

void MainWindow::buildMenuBar()
{
    effectsMenuBar_ = new EffectsMenuBar{this};
    this->setMenuBar(effectsMenuBar_);

    QMenu* fileMenu{this->menuBar()->addMenu(tr("&File"))};

    fileMenu->addAction(tr("&New"), documentManager_, SLOT(close()), QKeySequence::New);
    fileMenu->addAction(tr("&Open"), documentManager_, SLOT(open()), QKeySequence::Open);

    QMenu* recentFilesMenu{new QMenu(tr("Open &Recent..."))};
    recentFilesMenu->addAction(tr("Reopen Closed File"), documentManager_, SLOT(reopenLastClosedFile()), QKeySequence("SHIFT+CTRL+T"));
    recentFilesMenu->addSeparator();

    for (int i{0}; i < MAX_RECENT_FILES; ++i) {
        recentFilesMenu->addAction(recentFilesActions_[i]);
    }

    recentFilesMenu->addSeparator();
    recentFilesMenu->addAction(tr("Clear Menu"), this, SLOT(clearRecentFileHistory()));

    fileMenu->addMenu(recentFilesMenu);

    fileMenu->addSeparator();
    fileMenu->addAction(tr("&Save"), documentManager_, SLOT(save()), QKeySequence::Save);
    fileMenu->addAction(tr("Save &As..."), documentManager_, SLOT(saveAs()), QKeySequence::SaveAs);
    fileMenu->addAction(tr("R&ename..."), documentManager_, SLOT(rename()));
    fileMenu->addAction(tr("Re&load from Disk..."), documentManager_, SLOT(reload()));
    fileMenu->addSeparator();
    fileMenu->addAction(tr("&Print"), htmlPreviewWidget_, SLOT(printPreview()), QKeySequence::Print);
    fileMenu->addAction(tr("&Export"), documentManager_, SLOT(exportFile()), QKeySequence("CTRL+E"));
    fileMenu->addSeparator();
    fileMenu->addAction(tr("&Quit"), this, SLOT(quitApplication()), QKeySequence("Ctrl+Q"));

    QMenu* editMenu{this->menuBar()->addMenu(tr("&Edit"))};
    editMenu->addAction(tr("&Undo"), editor_, SLOT(undo()), QKeySequence::Undo);
    editMenu->addAction(tr("&Redo"), editor_, SLOT(redo()), QKeySequence::Redo);
    editMenu->addSeparator();
    editMenu->addAction(tr("Cu&t"), editor_, SLOT(cut()), QKeySequence::Cut);
    editMenu->addAction(tr("&Copy"), editor_, SLOT(copy()), QKeySequence::Copy);
    editMenu->addAction(tr("&Paste"), editor_, SLOT(paste()), QKeySequence::Paste);
    editMenu->addSeparator();
    editMenu->addAction(tr("&Insert Image..."), this, SLOT(insertImage()));
    editMenu->addSeparator();
    editMenu->addAction(tr("&Find"), findReplaceDialog_, SLOT(showFindMode()), QKeySequence::Find);
    editMenu->addAction(tr("Rep&lace"), findReplaceDialog_, SLOT(showReplaceMode()), QKeySequence("Ctrl+R"));
    editMenu->addSeparator();
    editMenu->addAction(tr("&Spell check"), editor_, SLOT(runSpellChecker()));

    QMenu* formatMenu{this->menuBar()->addMenu(tr("For&mat"))};
    formatMenu->addAction(tr("&Bold"), editor_, SLOT(bold()), QKeySequence::Bold);
    formatMenu->addAction(tr("&Italic"), editor_, SLOT(italic()), QKeySequence::Italic);
    formatMenu->addAction(tr("Stri&kethrough"), editor_, SLOT(strikethrough()), QKeySequence("Ctrl+K"));
    formatMenu->addAction(tr("HTML &Comment"), editor_, SLOT(insertComment()), QKeySequence("Ctrl+/"));
    formatMenu->addSeparator();
    formatMenu->addAction(tr("I&ndent"), editor_, SLOT(indentText()), QKeySequence("Tab"));
    formatMenu->addAction(tr("&Unindent"), editor_, SLOT(unindentText()), QKeySequence("Shift+Tab"));
    formatMenu->addSeparator();
    formatMenu->addAction(tr("Block &Quote"), editor_, SLOT(createBlockquote()), QKeySequence("Ctrl+."));
    formatMenu->addAction(tr("&Strip Block Quote"), editor_, SLOT(removeBlockquote()), QKeySequence("Ctrl+,"));
    formatMenu->addSeparator();
    formatMenu->addAction(tr("Code S&pan"), editor_, SLOT(createCodeSpan()), QKeySequence("Ctrl+J"));
    formatMenu->addAction(tr("Code B&lock"), editor_, SLOT(createCodeBlock()), QKeySequence("Ctrl+U"));
    formatMenu->addSeparator();
    formatMenu->addAction(tr("&* Bullet List"), editor_, SLOT(createBulletListWithAsteriskMarker()), QKeySequence("Ctrl+*"));
    formatMenu->addAction(tr("&- Bullet List"), editor_, SLOT(createBulletListWithMinusMarker()), QKeySequence("Ctrl+-"));
    formatMenu->addAction(tr("&+ Bullet List"), editor_, SLOT(createBulletListWithPlusMarker()), QKeySequence("Ctrl++"));
    formatMenu->addSeparator();
    formatMenu->addAction(tr("1&. Numbered List"), editor_, SLOT(createNumberedListWithPeriodMarker()), QKeySequence("Ctrl+\\"));
    formatMenu->addAction(tr("&1) Numbered List"), editor_, SLOT(createNumberedListWithParenthesisMarker()), QKeySequence("Ctrl+]"));
    formatMenu->addSeparator();
    formatMenu->addAction(tr("&Task List"), editor_, SLOT(createTaskList()), QKeySequence("Ctrl+T"));
    formatMenu->addAction(tr("Toggle Task(s) C&omplete"), editor_, SLOT(toggleTaskComplete()), QKeySequence("Ctrl+Y"));
    formatMenu->addAction(tr("&Horizontal Rule"), editor_, SLOT(insertRule()), QKeySequence("Ctrl+H"));

    QMenu* viewMenu{this->menuBar()->addMenu(tr("&View"))};

    fullScreenMenuAction_ = new QAction(tr("&Full Screen"), this);
    fullScreenMenuAction_->setCheckable(true);
    fullScreenMenuAction_->setChecked(this->isFullScreen());
    fullScreenMenuAction_->setShortcut(QKeySequence("F11"));
    connect(fullScreenMenuAction_, SIGNAL(toggled(bool)), this, SLOT(toggleFullscreen(bool)));
    viewMenu->addAction(fullScreenMenuAction_);

    viewMenu->addAction(tr("&Spell Book"), this, SLOT(showSpellBook()), QKeySequence("Ctrl+`"));
    viewMenu->addAction(tr("&Outline"), this, SLOT(showOutline()), QKeySequence("Ctrl+Shift+`"));
    viewMenu->addAction(tr("&Editor"), this, SLOT(showEditor()), QKeySequence("Ctrl+D"));
    viewMenu->addAction(tr("&HTML Preview"), this, SLOT(openHtmlPreview()), QKeySequence("Ctrl+Shift+D"));
    viewMenu->addSeparator();

    QMenu* settingsMenu{this->menuBar()->addMenu(tr("&Settings"))};
    settingsMenu->addAction(tr("Themes..."), this, SLOT(changeTheme()));
    settingsMenu->addAction(tr("Font..."), this, SLOT(changeFont()));

    bool autoHideEnabled{appSettings_->getHideMenuBarInFullScreenEnabled()};
    QAction* autoHideAction{new QAction(tr("Hide menu bar in full screen mode"), this)};
    autoHideAction->setCheckable(true);
    autoHideAction->setChecked(autoHideEnabled);
    connect(autoHideAction, SIGNAL(toggled(bool)), this, SLOT(toggleHideMenuBarInFullScreen(bool)));
    settingsMenu->addAction(autoHideAction);

    bool autoMatchEnabled{appSettings_->getAutoMatchEnabled()};
    QAction* autoMatchAction{new QAction(tr("Automatically Match Characters while Typing"), this)};
    autoMatchAction->setCheckable(true);
    autoMatchAction->setChecked(autoMatchEnabled);
    connect(autoMatchAction, SIGNAL(toggled(bool)), this, SLOT(toggleAutoMatch(bool)));
    settingsMenu->addAction(autoMatchAction);

    settingsMenu->addAction(tr("Customize Matched Characters..."), this, SLOT(showAutoMatchFilterDialog()));

    bool bulletCyclingEnabled{appSettings_->getBulletPointCyclingEnabled()};
    QAction* bulletCycleAction{new QAction(tr("Cycle Bullet Point Markers"), this)};
    bulletCycleAction->setCheckable(true);
    bulletCycleAction->setChecked(bulletCyclingEnabled);
    connect(bulletCycleAction, SIGNAL(toggled(bool)), this, SLOT(toggleBulletPointCycling(bool)));
    settingsMenu->addAction(bulletCycleAction);

    settingsMenu->addSeparator();

    QAction* liveSpellcheckAction{new QAction(tr("Live Spellcheck Enabled"), this)};
    liveSpellcheckAction->setCheckable(true);
    liveSpellcheckAction->setChecked(appSettings_->getLiveSpellCheckEnabled());
    connect(liveSpellcheckAction, SIGNAL(toggled(bool)), this, SLOT(toggleLiveSpellCheck(bool)));
    settingsMenu->addAction(liveSpellcheckAction);

    settingsMenu->addAction(tr("Dictionaries..."), this, SLOT(onSetDictionary()));

    settingsMenu->addAction(tr("Application Language..."), this, SLOT(onSetLocale()));

    settingsMenu->addSeparator();

    QAction* fileHistoryAction{new QAction(tr("Remember File History"), this)};
    fileHistoryAction->setCheckable(true);
    fileHistoryAction->setChecked(appSettings_->getFileHistoryEnabled());
    connect(fileHistoryAction, SIGNAL(toggled(bool)), this, SLOT(toggleFileHistoryEnabled(bool)));
    settingsMenu->addAction(fileHistoryAction);

    settingsMenu->addSeparator();

    settingsMenu->addAction(tr("Tabulation Width..."), this, SLOT(changeTabulationWidth()));
    editor_->setTabulationWidth(appSettings_->getTabWidth());

    QMenu* helpMenu{this->menuBar()->addMenu(tr("&Help"))};
    helpMenu->addAction(tr("&About"), this, SLOT(showAbout()));
    helpMenu->addAction(tr("About &Qt"), qApp, SLOT(aboutQt()));
    helpMenu->addAction(tr("Quick &Reference Guide"), this, SLOT(showQuickReferenceGuide()));

    connect(fileMenu, SIGNAL(aboutToShow()), effectsMenuBar_, SLOT(onAboutToShow()));
    connect(fileMenu, SIGNAL(aboutToHide()), effectsMenuBar_, SLOT(onAboutToHide()));
    connect(editMenu, SIGNAL(aboutToShow()), effectsMenuBar_, SLOT(onAboutToShow()));
    connect(editMenu, SIGNAL(aboutToHide()), effectsMenuBar_, SLOT(onAboutToHide()));
    connect(formatMenu, SIGNAL(aboutToShow()), effectsMenuBar_, SLOT(onAboutToShow()));
    connect(formatMenu, SIGNAL(aboutToHide()), effectsMenuBar_, SLOT(onAboutToHide()));
    connect(viewMenu, SIGNAL(aboutToShow()), effectsMenuBar_, SLOT(onAboutToShow()));
    connect(viewMenu, SIGNAL(aboutToHide()), effectsMenuBar_, SLOT(onAboutToHide()));
    connect(settingsMenu, SIGNAL(aboutToShow()), effectsMenuBar_, SLOT(onAboutToShow()));
    connect(settingsMenu, SIGNAL(aboutToHide()), effectsMenuBar_, SLOT(onAboutToHide()));
    connect(helpMenu, SIGNAL(aboutToShow()), effectsMenuBar_, SLOT(onAboutToShow()));
    connect(helpMenu, SIGNAL(aboutToHide()), effectsMenuBar_, SLOT(onAboutToHide()));
}

void MainWindow::buildStatusBar()
{
    centralStatusBar_ = new QFrame();
    centralStatusBar_->setFixedHeight(32);
    QHBoxLayout* statusBarLayout{new QHBoxLayout{centralStatusBar_}};

    statusLabel_ = new QLabel();
    statusBarLayout->addWidget(statusLabel_, 0, Qt::AlignLeft);
    statusLabel_->setStyleSheet("color: white; background-color: black; padding: 0px; margin: 0px");
    statusLabel_->hide();

    wordCountLabel_ = new QLabel();
    wordCountLabel_->setAlignment(Qt::AlignCenter);
    wordCountLabel_->setFrameShape(QFrame::NoFrame);
    wordCountLabel_->setLineWidth(0);
    wordCountLabel_->setStyleSheet("background: transparent; border: 0px; padding: 0px; margin: 0px; max-height: 32px");
    updateWordCount(0);
    statusBarLayout->addWidget(wordCountLabel_, 0, Qt::AlignCenter);

    statusBarLayout->setSpacing(0);
    centralStatusBar_->setLayout(statusBarLayout);
    centralStatusBar_->setObjectName("statusbar");
    verticalLayout2_->addWidget(centralStatusBar_);
}

void MainWindow::applyTheme()
{
    if (!theme_.getName().isNull() && !theme_.getName().isEmpty())
        appSettings_->setThemeName(theme_.getName());

    if (EditorAspectStretch == theme_.getEditorAspect())
        theme_.setBackgroundColor(theme_.getEditorBackgroundColor());

    QString styleSheet{};
    QTextStream stream{&styleSheet};

    QColor scrollBarColor{theme_.getDefaultTextColor()};
    scrollBarColor.setAlpha(128);
    scrollBarColor = ColorHelper::applyAlpha(scrollBarColor, theme_.getEditorBackgroundColor());

    QString scrollbarColorRGB{ColorHelper::toRgbString(scrollBarColor)};

    QString backgroundColorRGBA{};

    if (EditorAspectStretch == theme_.getEditorAspect())
        backgroundColorRGBA = "transparent";
    else
        backgroundColorRGBA = ColorHelper::toRgbaString(theme_.getEditorBackgroundColor());

    QString editorSelectionFgColorRGB{ColorHelper::toRgbString(theme_.getEditorBackgroundColor())};

    QString editorSelectionBgColorRGB{ColorHelper::toRgbString(theme_.getSelectionColor())};

    QString menuBarItemFgColorRGB{};
    QString menuBarItemBgColorRGBA{};
    QString menuBarItemFgPressColorRGB{};
    QString menuBarItemBgPressColorRGBA{};

    QString restoreIcon{};
    QString restoreIconHover{};
    QString restoreIconPressed{};

    QString statusBarItemFgColorRGB{};
    QString statusBarButtonFgPressHoverColorRGB{};
    QString statusBarButtonBgPressHoverColorRGBA{};

    if (EditorAspectStretch == theme_.getEditorAspect()) {
        QColor fadedTextColor{theme_.getDefaultTextColor()};
        fadedTextColor.setAlpha(165);
        fadedTextColor = ColorHelper::applyAlpha(fadedTextColor, theme_.getBackgroundColor());

        menuBarItemFgColorRGB = ColorHelper::toRgbString(fadedTextColor);
        menuBarItemFgColorRGB = ColorHelper::toRgbString(fadedTextColor);
        menuBarItemBgColorRGBA = "transparent";
        menuBarItemFgPressColorRGB = ColorHelper::toRgbString(theme_.getEditorBackgroundColor());
        menuBarItemBgPressColorRGBA = ColorHelper::toRgbaString(fadedTextColor);

        restoreIcon = ":/resources/images/view-restore-dark.svg";
        restoreIconHover = ":/resources/images/view-restore-dark-hover.svg";
        restoreIconPressed = ":/resources/images/view-restore-dark-hover.svg";

        statusBarItemFgColorRGB = menuBarItemFgColorRGB;
        statusBarButtonFgPressHoverColorRGB = ColorHelper::toRgbString(theme_.getEditorBackgroundColor());
        statusBarButtonBgPressHoverColorRGBA = menuBarItemBgPressColorRGBA;
    } else {
        QColor chromeFgColor{"#F2F2F2"};

        menuBarItemFgColorRGB = ColorHelper::toRgbString(chromeFgColor);
        menuBarItemBgColorRGBA = "transparent";
        menuBarItemFgPressColorRGB = menuBarItemFgColorRGB;
        chromeFgColor.setAlpha(80);
        menuBarItemBgPressColorRGBA = ColorHelper::toRgbaString(chromeFgColor);

        restoreIcon = ":/resources/images/view-restore-light.svg";
        restoreIconHover = ":/resources/images/view-restore-light-hover.svg";
        restoreIconPressed = ":/resources/images/view-restore-light-hover.svg";

        statusBarItemFgColorRGB = menuBarItemFgColorRGB;
        statusBarButtonFgPressHoverColorRGB = menuBarItemFgPressColorRGB;
        statusBarButtonBgPressHoverColorRGBA = menuBarItemBgPressColorRGBA;
    }

    editor_->setAspect(theme_.getEditorAspect());

    styleSheet = "";

    QString corners{""};

    if((EditorCornersRounded == theme_.getEditorCorners()) && (EditorAspectStretch != theme_.getEditorAspect()))
        corners = "border-radius: 10;";

    QString cursorColorRGB{ColorHelper::toRgbString(theme_.getCursorColor())};

    stream
        << "QPlainTextEdit { border: 0; "
        << corners
        << "margin: 0; padding: 5px; background-color: "
        << backgroundColorRGBA
        // The following two lines only set the cursor color, since the
        // Highlighter always colors the default character format for each block
        // with the theme's default text color.
        << "; color: "
        << cursorColorRGB
        << "; selection-color: "
        << editorSelectionFgColorRGB
        << "; selection-background-color: "
        << editorSelectionBgColorRGB
        << " } "
        << "QAbstractScrollArea::corner { background: transparent } "
        << "QScrollBar::horizontal { border: 0; background: transparent; height: 10px; margin: 0 } "
        << "QScrollBar::handle:horizontal { border: 0; background: "
        << scrollbarColorRGB
        << "; min-width: 50px; border-radius: 5px; } "
        << "QScrollBar::vertical { border: 0; background: transparent; width: 10px; margin: 0 } "
        << "QScrollBar::handle:vertical { border: 0; background: "
        << scrollbarColorRGB
        << "; min-height: 50px; border-radius: 5px;} "
        << "QScrollBar::add-line { background: transparent; border: 0 } "
        << "QScrollBar::sub-line { background: transparent; border: 0 } " ;

    editor_->setColorScheme(theme_.getDefaultTextColor(),
                            theme_.getSelectionColor(),
                            theme_.getCursorColor(),
                            theme_.getEditorBackgroundColor(),
                            theme_.getMarkdownColor(),
                            theme_.getLinkColor(),
                            theme_.getSpellingErrorColor());

    editor_->setStyleSheet(styleSheet);

    styleSheet = "";

    stream  << "QCheckBox { background: transparent; padding: 3 3 3 3; margin: 0 5 0 5 } "
            << "QCheckBox::indicator { width: 20px; height: 20px; background: transparent } "
            << "QCheckBox::indicator:checked { image: url("
            << restoreIcon
            << ") } "
            << "QCheckBox::indicator:checked:hover { image: url("
            << restoreIconHover
            << ") } "
            << "QCheckBox::indicator:checked:pressed { image: url("
            << restoreIconHover
            << ") } " ;

    if (EditorAspectCenter == theme_.getEditorAspect()) {
        QGraphicsDropShadowEffect* chromeDropShadowEffect = new QGraphicsDropShadowEffect();
        chromeDropShadowEffect->setColor(QColor(Qt::black));
        chromeDropShadowEffect->setBlurRadius(3.5);
        chromeDropShadowEffect->setXOffset(1.0);
        chromeDropShadowEffect->setYOffset(1.0);

        this->statusBar()->setGraphicsEffect(chromeDropShadowEffect);
        effectsMenuBar_->setDropShadow(Qt::black, 3.5, 1.0, 1.0);
    } else {
        this->statusBar()->setGraphicsEffect(NULL);
        effectsMenuBar_->removeDropShadow();
    }

    styleSheet = "";

    // Wipe out old background image drawing material.
    originalBackgroundImage_ = QImage{};
    adjustedBackgroundImage_ = QImage{};

    if (!theme_.getBackgroundImageUrl().isNull() && !theme_.getBackgroundImageUrl().isEmpty()) {
        // Predraw background image for paintEvent()
        originalBackgroundImage_.load(theme_.getBackgroundImageUrl());
        predrawBackgroundImage();
    }

    stream  << "#editorLayoutArea { background-color: transparent; border: 0; margin: 0 }"
            << "QMenuBar { background: transparent } "
            << "QMenuBar::item { background: "
            << menuBarItemBgColorRGBA
            << "; color: "
            << menuBarItemFgColorRGB
            << "; padding: 4px } "
            << "QMenuBar::item:pressed { background-color: "
            << menuBarItemBgPressColorRGBA
            << "; color: "
            << menuBarItemFgPressColorRGB
            << "; border-radius: 3px"
            << " } " ;

    setStyleSheet(styleSheet);

    // Make the word count and focus mode button font size
    // match the menu bar's font size, since on Windows using
    // the default QLabel and QPushButton font sizes results in
    // tiny font sizes for these.  We need them to stand out
    // a little bit more than a typical label or button.
    //
    int menuBarFontSize{this->menuBar()->fontInfo().pointSize()};

    styleSheet = "";

    stream  << "QStatusBar { margin: 0; padding: 0; border: 0; background: transparent } "
            << "QStatusBar::item { border: 0 } "
            << "QLabel { font-size: "
            << menuBarFontSize
            << "pt; margin: 0; padding: 0; border: 0; background: transparent; color: "
            << statusBarItemFgColorRGB
            << " } "
            << "QPushButton { font-size: "
            << menuBarFontSize
            << "pt; padding: 5px; border: 0; border-radius: 5px; background: transparent"
            << "; color: "
            << statusBarItemFgColorRGB
            << " } "
            << "QPushButton:pressed, QPushButton:flat, QPushButton:checked, QPushButton:hover { margin: 0; color: "
            << statusBarButtonFgPressHoverColorRGB
            << "; background-color: "
            << statusBarButtonBgPressHoverColorRGBA
            << " } " ;

    statusBar()->setStyleSheet(styleSheet);

    htmlPreviewWidget_->statusBar_->setStyleSheet(styleSheet);

    styleSheet = "";

    // Style the Panels
    QColor defaultTextColor{theme_.getDefaultTextColor()};
    QColor selectionColor{theme_.getSelectionColor()};
    QColor cursorColor{theme_.getCursorColor()};
    QColor markdownColor{theme_.getMarkdownColor()};
    QColor linkColor{theme_.getLinkColor()};
    QColor backgroundColor{theme_.getEditorBackgroundColor()};

    QString defaultTextStringRGB{ColorHelper::toRgbString(defaultTextColor)};
    QString selectionStringRGB{ColorHelper::toRgbString(selectionColor)};
    QString cursorStringRGB{ColorHelper::toRgbString(cursorColor)};
    QString markdownStringRGB{ColorHelper::toRgbString(markdownColor)};
    QString linkStringRGB{ColorHelper::toRgbString(linkColor)};
    QString backgroundStringRGB{ColorHelper::toRgbString(backgroundColor)};

    // Important!  For QListWidget (used in Outline HUD), set
    // QListWidget { outline: none } for the style sheet to get rid of the
    // focus rectangle without losing keyboard focus capability.
    // Unforntunately, this property isn't in the Qt documentation, so
    // it's being documented here for posterity's sake.
    //
    for (bool spellBook : {false, true}){
        stream  << "QListWidget { outline: none; border: 0; padding: 1; background-color:"
                << backgroundStringRGB
                << "; color: "
                << (spellBook ? markdownStringRGB : defaultTextStringRGB)
                << " } QListWidget::item { padding: 1 0 1 0; margin: 0; background-color: transparent } "
                << "QListWidget::item:selected { border-radius: 3px; color: "
                << backgroundStringRGB
                << "; background-color: "
                << (spellBook ? markdownStringRGB : cursorStringRGB)
                << " }"
                << "QScrollBar::horizontal { border: 0; background: transparent; height: 10px; margin: 0 } "
                << "QScrollBar::handle:horizontal { border: 0; background: "
                << scrollbarColorRGB
                << "; min-width: 20px; border-radius: 5px; } "
                << "QScrollBar::vertical { border: 0; background: transparent; width: 10px; margin: 0 } "
                << "QScrollBar::handle:vertical { border: 0; background: "
                << scrollbarColorRGB
                << "; min-height: 20px; border-radius: 5px;} "
                << "QScrollBar::add-line { background: transparent; border: 0 } "
                << "QScrollBar::sub-line { background: transparent; border: 0 } "
                << "QAbstractScrollArea::corner { background: transparent } " ;

        spellBook ? spellBookWidget_->setStyleSheet(styleSheet)
                  : outlineWidget_->setStyleSheet(styleSheet);
    }

    editor_->setupPaperMargins(this->width());

//    htmlPreviewWidget_->updateStatusBarStyle();
}

// Lifted from FocusWriter's theme.cpp file
void MainWindow::predrawBackgroundImage()
{
    adjustedBackgroundImage_ = QImage(this->size(), QImage::Format_ARGB32_Premultiplied);
    adjustedBackgroundImage_.fill(theme_.getBackgroundColor().rgb());

    QPainter painter{&adjustedBackgroundImage_};
    painter.setPen(Qt::NoPen);

    if (PictureAspectTile == theme_.getBackgroundImageAspect()) {
        painter.fillRect(adjustedBackgroundImage_.rect(), originalBackgroundImage_);
    } else {
        Qt::AspectRatioMode aspectRatioMode{Qt::IgnoreAspectRatio};
        bool scaleImage{true};

        switch (theme_.getBackgroundImageAspect()) {
        case PictureAspectZoom:
            aspectRatioMode = Qt::KeepAspectRatioByExpanding;
            break;
        case PictureAspectScale:
            aspectRatioMode = Qt::KeepAspectRatio;
            break;
        case PictureAspectStretch:
            aspectRatioMode = Qt::IgnoreAspectRatio;
            break;
        default:
            // Centered
            scaleImage = false;
            break;
        }

        QImage image{originalBackgroundImage_};

        if (scaleImage)
            image = image.scaled(this->size(), aspectRatioMode, Qt::SmoothTransformation);

        painter.drawImage((this->width() - image.width()) / 2, (this->height() - image.height()) / 2, image);

        painter.end();
    }
}
