<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DictionaryDialog</name>
    <message>
        <location filename="../src/spelling/dictionary_dialog.cpp" line="36"/>
        <source>Set Dictionary</source>
        <translation>Выбрать словарь</translation>
    </message>
</context>
<context>
    <name>DocumentManager</name>
    <message>
        <location filename="../src/DocumentManager.cpp" line="159"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="174"/>
        <source>Could not open %1</source>
        <translation>Не удалось открыть %1</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="175"/>
        <source>Permission denied.</source>
        <translation type="unfinished">Нет прав на чтение.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="251"/>
        <source>The document has been modified.</source>
        <translation>Документ был изменён.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="252"/>
        <source>Discard changes?</source>
        <translation type="unfinished">Откатить изменения?</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="286"/>
        <source>Rename File</source>
        <translation>Переименовать файл</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="301"/>
        <source>Failed to rename %1</source>
        <translation>Не удалось переименовать %1</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="372"/>
        <source>Save File</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="476"/>
        <source>Error saving %1</source>
        <translation>Ошибка при сохранении %1</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="536"/>
        <source>The document has been modified by another program.</source>
        <translation type="unfinished">Документ был изменён другой программой.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="537"/>
        <source>Would you like to reload the document?</source>
        <translation type="unfinished">Желаете заново загрузить документ?</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="594"/>
        <location filename="../src/DocumentManager.cpp" line="633"/>
        <source>Could not read %1</source>
        <translation>Не удалось прочесть %1</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="608"/>
        <source>opening %1</source>
        <translation>отркрываю %1</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="730"/>
        <source>File has been modified.</source>
        <translation type="unfinished">Файл был изменён.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="734"/>
        <source>%1 has been modified.</source>
        <translation type="unfinished">%1 был изменён.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="743"/>
        <source>Would you like to save your changes?</source>
        <translation type="unfinished">Желаете сохранить свои правки?</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="779"/>
        <source>%1 is read only.</source>
        <translation type="unfinished">%1 доступен только для чтения.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="780"/>
        <source>Overwrite protected file?</source>
        <translation type="unfinished">Переписать защищённый файл?</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="806"/>
        <source>Overwrite failed.</source>
        <translation type="unfinished">Не удалось перезаписать.</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="807"/>
        <source>Please save file to another location.</source>
        <translation type="unfinished">Пожалуйста, сохраните файл в другую локацию.</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/ExportDialog.cpp" line="49"/>
        <source>Export</source>
        <translation>Экспортировать</translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="189"/>
        <source>Smart Typography</source>
        <translation>Умная типография</translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="192"/>
        <source>Export Options</source>
        <translation>Настройки экспорта</translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="194"/>
        <source>Markdown Converter:</source>
        <translation type="unfinished">Преобразователь Markdown:</translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="237"/>
        <source>exporting to %1</source>
        <translation type="unfinished">экспортирую в %1</translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="254"/>
        <source>Export failed.</source>
        <translation type="unfinished">Не удалось экспортировать.</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <location filename="../src/find_dialog.cpp" line="45"/>
        <source>Search for:</source>
        <translation>Найти:</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="47"/>
        <source>Replace with:</source>
        <translation>Заменить на:</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="51"/>
        <source>Ignore case</source>
        <translation>Без учёта регистра</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="52"/>
        <source>Whole words only</source>
        <translation>Слова целиком</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="53"/>
        <source>Regular expressions</source>
        <translation>Регулярные выражения</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="56"/>
        <source>Search up</source>
        <translation>Искать выше</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="57"/>
        <source>Search down</source>
        <translation>Искать ниже</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="64"/>
        <source>&amp;Find</source>
        <translation>&amp;Найти</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="68"/>
        <source>&amp;Replace</source>
        <translation>&amp;Заменить</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="72"/>
        <source>Replace &amp;All</source>
        <translation>Заменить &amp;всё</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="134"/>
        <source>Find</source>
        <translation>Найти</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="142"/>
        <source>Replace</source>
        <translation>Заменить</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="277"/>
        <source>Question</source>
        <translation>Вопрос</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/find_dialog.cpp" line="277"/>
        <source>Replace %n instance(s)?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="281"/>
        <location filename="../src/find_dialog.cpp" line="355"/>
        <source>Sorry</source>
        <translation>К сожалению</translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="281"/>
        <location filename="../src/find_dialog.cpp" line="355"/>
        <source>Phrase not found.</source>
        <translation>Фраза не найдена.</translation>
    </message>
</context>
<context>
    <name>HtmlPreview</name>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="70"/>
        <source>HTML Preview</source>
        <translation>Предпросмотр HTML</translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="101"/>
        <source>Copy HTML</source>
        <translation>Копировать HTML</translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="106"/>
        <source>Export</source>
        <translation>Экспорт</translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="111"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="123"/>
        <source>No markdown (pandoc, multimarkdown, discount) processors are installed.  Please install or add their installation locations to your system PATH environment variable.</source>
        <translation type="unfinished">Не установлено ни одного обработчика markdown (pandoc, multimarkdown, discount). Пожалуйста, установите их или добавьте пути установки в переменную окружения PATH.</translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="151"/>
        <source>Github (Default)</source>
        <translation>Github (по-умолчанию)</translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="170"/>
        <source>Add/Remove Custom Style Sheets...</source>
        <translation type="unfinished">Добавить/удалить собственные таблицы стилей...</translation>
    </message>
</context>
<context>
    <name>ImageButton</name>
    <message>
        <location filename="../src/image_button.cpp" line="90"/>
        <source>Open Image</source>
        <translation>Открыть изображение</translation>
    </message>
    <message>
        <location filename="../src/image_button.cpp" line="90"/>
        <source>Images(%1)</source>
        <translation>Изображения(%1)</translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="../src/LocaleDialog.cpp" line="37"/>
        <source>Set Application Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/LocaleDialog.cpp" line="48"/>
        <source>The translations folder is missing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/LocaleDialog.cpp" line="49"/>
        <source>Please reinstall this application for more language options.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/MainWindow.cpp" line="98"/>
        <source>Outline</source>
        <translation>Содержание</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="110"/>
        <source># Heading 1</source>
        <translation># Заголовок 1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="111"/>
        <source>## Heading 2</source>
        <translation>## Заголовок 2</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="112"/>
        <source>### Heading 3</source>
        <translation>### Заголовок 3</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="113"/>
        <source>#### Heading 4</source>
        <translation>#### Заголовок 4</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="114"/>
        <source>##### Heading 5</source>
        <translation>##### Заголовок 5</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="115"/>
        <source>###### Heading 6</source>
        <translation>###### Заголовок 6</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="116"/>
        <source>*Emphasis* _Emphasis_</source>
        <translation type="unfinished">*Курсив* _Курсив_</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="117"/>
        <source>**Strong** __Strong__</source>
        <translation type="unfinished">**Жирный** __Жирный__</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="118"/>
        <source>1. Numbered List</source>
        <translation>1. Нумерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="119"/>
        <source>* Bullet List</source>
        <translation>* Маркерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="120"/>
        <source>+ Bullet List</source>
        <translation>+ Маркерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="121"/>
        <source>- Bullet List</source>
        <translation>- Маркерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="122"/>
        <source>&gt; Block Quote</source>
        <translation>&gt; Блочная цитата</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="123"/>
        <source>`Code Span`</source>
        <translation>`Код`</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="124"/>
        <source>``` Code Block</source>
        <translation>``` Блок кода</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="125"/>
        <source>[Link](http://url.com &quot;Title&quot;]</source>
        <translation>[Ссылка](http://url.com &quot;Заголовок&quot;)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="126"/>
        <source>[Reference Link][ID]</source>
        <translatorcomment>Коряво!</translatorcomment>
        <translation type="unfinished">[Ссылка внутри документа][Идентификатор]</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="127"/>
        <source>![Image][./image.jpg &quot;Title&quot;]</source>
        <translation>![Изображение][./image.jpg &quot;Заголовок&quot;]</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="128"/>
        <source>--- *** ___ Horizontal Rule</source>
        <translation>--- *** ___ Горизонтальная черта</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="131"/>
        <source>Cheat Sheet</source>
        <translation>Шпаргалка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="687"/>
        <source>Insert Image</source>
        <translation>Вставить изображение</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="690"/>
        <source>Images</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="691"/>
        <source>All Files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="728"/>
        <source>Tabulation Width</source>
        <translation>Ширина табуляции</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="729"/>
        <source>Spaces</source>
        <translation>Пробелы</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="784"/>
        <source>Failed to open Quick Reference Guide.</source>
        <translation>Не удалось открыть краткую справку.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="800"/>
        <source>Quick Reference Guide</source>
        <translation>Краткая справка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="852"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2014-2016 wereturtle&lt;/b&gt;&lt;p&gt;You may use and redistribute this software under the terms of the &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;GNU General Public License Version 3&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Visit the official website at &lt;a href=&quot;http://github.com/wereturtle/ghostwriter&quot;&gt;http://github.com/wereturtle/ghostwriter&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Special thanks and credit for reused code goes to&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;mailto:graeme@gottcode.org&quot;&gt;Graeme Gott&lt;/a&gt;, author of &lt;a href=&quot;http://gottcode.org/focuswriter/&quot;&gt;FocusWriter&lt;/a&gt;&lt;br/&gt;Dmitry Shachnev, author of &lt;a href=&quot;http://sourceforge.net/p/retext/home/ReText/&quot;&gt;Retext&lt;/a&gt;&lt;br/&gt;&lt;a href=&quot;mailto:gabriel@teuton.org&quot;&gt;Gabriel M. Beddingfield&lt;/a&gt;, author of &lt;a href=&quot;http://www.teuton.org/~gabriel/stretchplayer/&quot;&gt;StretchPlayer&lt;/a&gt;&lt;br/&gt;&lt;p&gt;I am also deeply indebted to &lt;a href=&quot;mailto:w.vollprecht@gmail.com&quot;&gt;Wolf Vollprecht&lt;/a&gt;, the author of &lt;a href=&quot;http://uberwriter.wolfvollprecht.de/&quot;&gt;UberWriter&lt;/a&gt;, for the inspiration he provided in creating such a beautiful Markdown editing tool.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="875"/>
        <source>About %1</source>
        <translation>О %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/MainWindow.cpp" line="880"/>
        <source>%n word(s)</source>
        <translation>
            <numerusform>%n слово</numerusform>
            <numerusform>%n слова</numerusform>
            <numerusform>%n слов</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1051"/>
        <source>Hud Window Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1205"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1207"/>
        <source>&amp;New</source>
        <translation>Со&amp;здать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1208"/>
        <source>&amp;Open</source>
        <translation>&amp;Открыть</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1211"/>
        <source>Reopen Closed File</source>
        <translation type="unfinished">Повторно открыть закрытый файл</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1220"/>
        <source>Clear Menu</source>
        <translation>Очистить список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1225"/>
        <source>&amp;Save</source>
        <translation>&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1226"/>
        <source>Save &amp;As...</source>
        <translation>Сохранить &amp;как...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1210"/>
        <source>Open &amp;Recent...</source>
        <translation>Открыть &amp;недавнее...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1039"/>
        <source>Please restart the application for changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1085"/>
        <source>Matched Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1227"/>
        <source>R&amp;ename...</source>
        <translation>Пере&amp;именовать...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1228"/>
        <source>Re&amp;load from Disk...</source>
        <translation>Загрузить &amp;заново с диска...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1230"/>
        <source>Print Pre&amp;view</source>
        <translation>П&amp;редпросмотр печати</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1231"/>
        <source>&amp;Print</source>
        <translation>&amp;Печать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1233"/>
        <source>&amp;Export</source>
        <translation>&amp;Экспортировать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1235"/>
        <source>&amp;Quit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1237"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1238"/>
        <source>&amp;Undo</source>
        <translation>&amp;Отменить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1239"/>
        <source>&amp;Redo</source>
        <translation>&amp;Повторить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1241"/>
        <source>Cu&amp;t</source>
        <translation>Выр&amp;езать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1242"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1243"/>
        <source>&amp;Paste</source>
        <translation>&amp;Вставить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1245"/>
        <source>&amp;Insert Image...</source>
        <translation>Вставить &amp;изображение...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1247"/>
        <source>&amp;Find</source>
        <translation>&amp;Найти</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1248"/>
        <source>Rep&amp;lace</source>
        <translation>&amp;Заменить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1250"/>
        <source>&amp;Spell check</source>
        <translation>Проверка о&amp;рфографии</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1252"/>
        <source>For&amp;mat</source>
        <translation>&amp;Разметка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1253"/>
        <source>&amp;Bold</source>
        <translation>&amp;Жирно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1254"/>
        <source>&amp;Italic</source>
        <translation>&amp;Курсив</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1255"/>
        <source>Stri&amp;kthrough</source>
        <translation>Пере&amp;чёркнуто</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1256"/>
        <source>&amp;HTML Comment</source>
        <translation>Комментарий &amp;HTML</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1258"/>
        <source>I&amp;ndent</source>
        <translation>&amp;Отступить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1259"/>
        <source>&amp;Unindent</source>
        <translation>&amp;Убрать отступ</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1261"/>
        <source>Block &amp;Quote</source>
        <translation>Блочная &amp;цитата</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1262"/>
        <source>&amp;Strip Block Quote</source>
        <translation>У&amp;брать цитирование</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1264"/>
        <source>&amp;* Bullet List</source>
        <translation>&amp;* Маркерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1265"/>
        <source>&amp;- Bullet List</source>
        <translation>&amp;- Маркерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1266"/>
        <source>&amp;+ Bullet List</source>
        <translation>&amp;+ Маркерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1268"/>
        <source>1&amp;. Numbered List</source>
        <translation>1&amp;. Нумерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1269"/>
        <source>1&amp;) Numbered List</source>
        <translation>1&amp;) Нумерованный список</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1271"/>
        <source>&amp;Task List</source>
        <translation>Список &amp;задач</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1272"/>
        <source>Toggle Task(s) &amp;Complete</source>
        <translation type="unfinished">&amp;Переключить статус задач(и)</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1274"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1276"/>
        <source>&amp;Full Screen</source>
        <translation>&amp;Полный экран</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1283"/>
        <source>&amp;Preview in HTML</source>
        <translation>Предпросмотр &amp;HTML</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1284"/>
        <source>&amp;Outline HUD</source>
        <translation type="unfinished">&amp;Содержание</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1285"/>
        <source>&amp;Cheat Sheet HUD</source>
        <translation type="unfinished">&amp;Шпаргалка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1288"/>
        <source>&amp;Settings</source>
        <translation>&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1289"/>
        <source>Themes...</source>
        <translation>Темы...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1290"/>
        <source>Font...</source>
        <translation>Шрифт...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1292"/>
        <source>Focus Mode</source>
        <translatorcomment>Назовём пока так. Есть варианты получше?</translatorcomment>
        <translation>Режим фокусировки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1297"/>
        <source>Sentence</source>
        <translation>Предложение</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1303"/>
        <source>Current Line</source>
        <translation>Строка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1309"/>
        <source>Three Lines</source>
        <translation>Три строки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1315"/>
        <source>Paragraph</source>
        <translation>Абзац</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1328"/>
        <source>Editor Width</source>
        <translation>Ширина редактора</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1332"/>
        <source>Narrow</source>
        <translation>Узко</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1338"/>
        <source>Medium</source>
        <translation>Средне</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1344"/>
        <source>Wide</source>
        <translation>Широко</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1350"/>
        <source>Full</source>
        <translation>Во всю ширину</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1363"/>
        <source>Blockquote Style</source>
        <translation>Стиль цитат</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1367"/>
        <source>Plain</source>
        <translation>Просто</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1373"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1379"/>
        <source>Fancy</source>
        <translation>Вычурно</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1392"/>
        <source>Hide menu bar in full screen mode</source>
        <translation>Скрывать меню в полноэкранном режиме</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1399"/>
        <source>Use Large Headings</source>
        <translation>Использовать большие заголовки</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1406"/>
        <source>Use Underline Instead of Italics for Emphasis</source>
        <translation>Подчёркивание вместо курсива для выделения</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1413"/>
        <source>Automatically Match Characters while Typing</source>
        <translation>Автомотически дополнять символы при наборе</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1419"/>
        <source>Customize Matched Characters...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1422"/>
        <source>Cycle Bullet Point Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1429"/>
        <source>Display Current Time in Full Screen Mode</source>
        <translation>Показывать время в полноэкранном режиме</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1437"/>
        <source>Live Spellcheck Enabled</source>
        <translation>Проверять орфографию на лету</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1443"/>
        <source>Dictionaries...</source>
        <translation>Словари...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1445"/>
        <source>Application Language...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1449"/>
        <source>Remember File History</source>
        <translation>Запоминать историю файла</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1455"/>
        <source>Auto Save</source>
        <translation>Автосохранение</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1461"/>
        <source>Backup File on Save</source>
        <translation>Создавать резервную копию при сохранении</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1470"/>
        <source>Insert Spaces for Tabs</source>
        <translation>Вставлять пробелы вместо табуляции</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1477"/>
        <source>Tabulation Width...</source>
        <translation>Ширина табуляции...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1482"/>
        <source>Alternate Row Colors in HUD Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1490"/>
        <source>HUD Window Button Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1494"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1500"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1511"/>
        <source>Enable Desktop Compositing Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1519"/>
        <source>HUD Window Opacity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1521"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1522"/>
        <source>&amp;About</source>
        <translation>&amp;О программе</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1523"/>
        <source>About &amp;Qt</source>
        <translation>О &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1524"/>
        <source>Quick &amp;Reference Guide</source>
        <translation>Краткая &amp;справка</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1567"/>
        <source>Focus</source>
        <translation>Фокус</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1569"/>
        <source>Toggle distraction free mode</source>
        <translatorcomment>Коряво! Нужно заменить на что-то другое.</translatorcomment>
        <translation type="unfinished">Переключить режим концентрации</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1577"/>
        <source>Toggle full screen mode</source>
        <translation type="unfinished">Переключить полноэкранный режим</translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../src/MarkdownEditor.cpp" line="125"/>
        <source>Add word to dictionary</source>
        <translation>Добавить слово в словарь</translation>
    </message>
    <message>
        <location filename="../src/MarkdownEditor.cpp" line="126"/>
        <source>Check spelling...</source>
        <translation>Проверить правописание...</translation>
    </message>
    <message>
        <location filename="../src/MarkdownEditor.cpp" line="548"/>
        <source>No spelling suggestions found</source>
        <translation>Вариантов правописания не найдено</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/CommandLineExporter.cpp" line="92"/>
        <location filename="../src/CommandLineExporter.cpp" line="96"/>
        <source>Export failed: </source>
        <translation>Ошибка экспорта:</translation>
    </message>
    <message>
        <location filename="../src/CommandLineExporter.cpp" line="114"/>
        <source>%1 format is not supported by this processor.</source>
        <translation type="unfinished">Формат %1 не поддрерживается обработчиком.</translation>
    </message>
    <message>
        <location filename="../src/CommandLineExporter.cpp" line="122"/>
        <source>Failed to execute command: </source>
        <translation>Не удалось выполнить команду:</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="52"/>
        <source>Markdown</source>
        <translation>Markdown</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="53"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="54"/>
        <source>All</source>
        <translation>Все</translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="835"/>
        <source>Null or empty file path provided for writing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Exporter.cpp" line="66"/>
        <source>Export to HTML is not supported with this processor.</source>
        <translation type="unfinished">Экспорт в HTML не поддерживается этим обработчиком.</translation>
    </message>
    <message>
        <location filename="../src/SundownExporter.cpp" line="127"/>
        <source>%1 format is unsupported by the Sundown processor.</source>
        <translation type="unfinished">Sundown не поддерживает формат %1.</translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="147"/>
        <source>The specified theme does not exist in the file system: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="252"/>
        <source>The specified theme is not available.  Try restarting the application.  If problem persists, please file a bug report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="322"/>
        <source>Could not delete %1 from theme.  Please try setting the theme file permissions to be writeable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="345"/>
        <source>&apos;%1&apos; already exists.  Please choose another name.</source>
        <translation type="unfinished">«%1» уже существует. Пожалуйста, выберите другое имя.</translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="355"/>
        <source>&apos;%1&apos; theme already exists.  Please choose another name.</source>
        <translation type="unfinished">Тема «%1»уже существует. Пожалуйста, выберите другое имя.</translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="390"/>
        <source>Failed to rename theme.  Please check file permissions.</source>
        <translation type="unfinished">Не удалось переменовать тему. Пожалуйста, проверьте права на файл.</translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="435"/>
        <source>Theme is read-only.  Please try renaming the theme, or setting the theme file to be writable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="472"/>
        <source>Failed to remove old theme image.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="504"/>
        <source>The old theme image file could not be removed from the theme directory.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="516"/>
        <source>Theme image file could not be copied to the theme directory.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="535"/>
        <source>Theme could not be saved to disk.  Please check file permissions or try renaming the theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="560"/>
        <source>Untitled 1</source>
        <translation type="unfinished">Без имени 1</translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="573"/>
        <source>Untitled %1</source>
        <translation type="unfinished">Без имени %1</translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="629"/>
        <location filename="../src/ThemeFactory.cpp" line="651"/>
        <location filename="../src/ThemeFactory.cpp" line="679"/>
        <source>Invalid or missing value for %1 provided.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="659"/>
        <source>Value for %1 is out of range.  Valid values are between %2 and %3, inclusive.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimpleFontDialog</name>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="44"/>
        <source>Family</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="72"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="75"/>
        <source>AaBbCcXxYyZz</source>
        <translation>АаБбВбЭэЮюЯя</translation>
    </message>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="79"/>
        <source>Preview</source>
        <translation>Предпросмотр</translation>
    </message>
</context>
<context>
    <name>SpellChecker</name>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="139"/>
        <source>Check Spelling</source>
        <translation>Проверить орфографию</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="147"/>
        <source>&amp;Add</source>
        <translation>&amp;Добавить</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="150"/>
        <source>&amp;Ignore</source>
        <translation>&amp;Пропустить</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="153"/>
        <source>I&amp;gnore All</source>
        <translation>П&amp;ропустить все</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="158"/>
        <source>&amp;Change</source>
        <translation>&amp;Исправить</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="161"/>
        <source>C&amp;hange All</source>
        <translation>И&amp;справить все</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="176"/>
        <source>Not in dictionary:</source>
        <translation>Не в словаре:</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="184"/>
        <source>Change to:</source>
        <translation>Исправить на:</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="200"/>
        <source>Checking spelling...</source>
        <translation>Проверяю правописание...</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="200"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="201"/>
        <source>Please wait</source>
        <translation>Подождите, пожалуйста</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="228"/>
        <source>Continue checking at beginning of file?</source>
        <translation>Продолжить проверку с начала файла?</translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="287"/>
        <source>Spell check complete.</source>
        <translation>Проверка орфографии завершена.</translation>
    </message>
</context>
<context>
    <name>StyleSheetManagerDialog</name>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="38"/>
        <source>Custom Style Sheets</source>
        <translatorcomment>Ох уж эти обыгрывание аббревиатур... Ну, пусть будет так.</translatorcomment>
        <translation type="unfinished">Собственные таблицы стилей</translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="65"/>
        <source>Add new style sheet</source>
        <translation type="unfinished">Добавить новую таблицу стилей</translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="67"/>
        <source>Remove selected style sheet(s) from list. (No files will be deleted from the hard disk.)</source>
        <translation type="unfinished">Убрать выбранную таблицу стилей из списка (ни одного файла не будет удалено с диска)</translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="113"/>
        <source>Select CSS File</source>
        <translation type="unfinished">Выбрать файл CSS</translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="115"/>
        <source>CSS</source>
        <translation>CSS</translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="115"/>
        <source>All</source>
        <translation type="unfinished">Все</translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="144"/>
        <source>Don&apos;t worry! No files will be deleted from the hard disk. But are you sure you wish to remove the selected style sheet(s) from the list?</source>
        <translation type="unfinished">Спокойно! Ни один файл не будет удалён с диска. Тем не менее, вы уверены, что хотите убрать выбранные таблицы стилей из списка?</translation>
    </message>
</context>
<context>
    <name>TextDocument</name>
    <message>
        <location filename="../src/TextDocument.cpp" line="36"/>
        <location filename="../src/TextDocument.cpp" line="68"/>
        <source>untitled</source>
        <translation>без названия</translation>
    </message>
</context>
<context>
    <name>ThemeEditorDialog</name>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="40"/>
        <source>Edit Theme</source>
        <translation type="unfinished">Изменить тему</translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="98"/>
        <source>Rounded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="99"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="103"/>
        <location filename="../src/ThemeEditorDialog.cpp" line="111"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="104"/>
        <location filename="../src/ThemeEditorDialog.cpp" line="110"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="108"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="109"/>
        <source>Tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="112"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="113"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="124"/>
        <source>Theme Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="137"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="138"/>
        <source>Markup Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="139"/>
        <source>Link Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="140"/>
        <source>Spelling Error Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="141"/>
        <source>Text Area Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="144"/>
        <source>Text Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="150"/>
        <source>Background Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="151"/>
        <source>Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="153"/>
        <source>Editor Aspect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="154"/>
        <source>Editor Corners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="155"/>
        <source>Editor Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="158"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="164"/>
        <source>HUD Foreground Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="165"/>
        <source>HUD Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="168"/>
        <source>HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="247"/>
        <source>Unable to save theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="265"/>
        <source>Failed to rename theme.</source>
        <translation type="unfinished">Не удалось переименовать тему.</translation>
    </message>
</context>
<context>
    <name>ThemeSelectionDialog</name>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="50"/>
        <source>Themes</source>
        <translation>Темы</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="96"/>
        <source>Edit...</source>
        <translation type="unfinished">Изменить...</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="158"/>
        <source>Unable to load theme.</source>
        <translation>Не удалось загрузить тему.</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="239"/>
        <source>Cannot delete theme.</source>
        <translation>Невозможно удалить тему.</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="240"/>
        <source>Sorry, this is a built-in theme that cannot be deleted.</source>
        <translation>Увы, но это встроенная тема которую нельзя удалить.</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="251"/>
        <source>Are you sure you want to permanently delete the &apos;%1&apos; theme?</source>
        <translation>Вы уверены, что хотите навсегда удалить тему «%1»?</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="269"/>
        <source>Failed to delete theme.</source>
        <translation>Не удалось удалить тему.</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="300"/>
        <source>Cannot edit theme.</source>
        <translation type="unfinished">Невозможно изменить тему.</translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="301"/>
        <source>Sorry, this is a built-in theme that cannot be edited.</source>
        <translation type="unfinished">Увы, но это встроенная тема которую нельзя изменить.</translation>
    </message>
</context>
</TS>
