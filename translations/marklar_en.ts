<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>DictionaryDialog</name>
    <message>
        <location filename="../src/spelling/dictionary_dialog.cpp" line="36"/>
        <source>Set Dictionary</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DocumentManager</name>
    <message>
        <location filename="../src/DocumentManager.cpp" line="159"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="174"/>
        <source>Could not open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="175"/>
        <source>Permission denied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="251"/>
        <source>The document has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="252"/>
        <source>Discard changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="286"/>
        <source>Rename File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="301"/>
        <source>Failed to rename %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="372"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="476"/>
        <source>Error saving %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="536"/>
        <source>The document has been modified by another program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="537"/>
        <source>Would you like to reload the document?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="594"/>
        <location filename="../src/DocumentManager.cpp" line="633"/>
        <source>Could not read %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="608"/>
        <source>opening %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="730"/>
        <source>File has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="734"/>
        <source>%1 has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="743"/>
        <source>Would you like to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="779"/>
        <source>%1 is read only.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="780"/>
        <source>Overwrite protected file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="806"/>
        <source>Overwrite failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="807"/>
        <source>Please save file to another location.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/ExportDialog.cpp" line="49"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="189"/>
        <source>Smart Typography</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="192"/>
        <source>Export Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="194"/>
        <source>Markdown Converter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="237"/>
        <source>exporting to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExportDialog.cpp" line="254"/>
        <source>Export failed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <location filename="../src/find_dialog.cpp" line="45"/>
        <source>Search for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="47"/>
        <source>Replace with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="51"/>
        <source>Ignore case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="52"/>
        <source>Whole words only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="53"/>
        <source>Regular expressions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="56"/>
        <source>Search up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="57"/>
        <source>Search down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="64"/>
        <source>&amp;Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="68"/>
        <source>&amp;Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="72"/>
        <source>Replace &amp;All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="134"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="142"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="277"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/find_dialog.cpp" line="277"/>
        <source>Replace %n instance(s)?</source>
        <translation type="unfinished">
            <numerusform>Replace %n instance?</numerusform>
            <numerusform>Replace %n instances?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="281"/>
        <location filename="../src/find_dialog.cpp" line="355"/>
        <source>Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/find_dialog.cpp" line="281"/>
        <location filename="../src/find_dialog.cpp" line="355"/>
        <source>Phrase not found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HtmlPreview</name>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="70"/>
        <source>HTML Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="101"/>
        <source>Copy HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="106"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="111"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="123"/>
        <source>No markdown (pandoc, multimarkdown, discount) processors are installed.  Please install or add their installation locations to your system PATH environment variable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="151"/>
        <source>Github (Default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/HtmlPreview.cpp" line="170"/>
        <source>Add/Remove Custom Style Sheets...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageButton</name>
    <message>
        <location filename="../src/image_button.cpp" line="90"/>
        <source>Open Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/image_button.cpp" line="90"/>
        <source>Images(%1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LocaleDialog</name>
    <message>
        <location filename="../src/LocaleDialog.cpp" line="37"/>
        <source>Select Application Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/LocaleDialog.cpp" line="48"/>
        <source>Translations folder is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/LocaleDialog.cpp" line="49"/>
        <source>Please reinstall this application for more locale options.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/MainWindow.cpp" line="98"/>
        <source>Outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="110"/>
        <source># Heading 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="111"/>
        <source>## Heading 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="112"/>
        <source>### Heading 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="113"/>
        <source>#### Heading 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="114"/>
        <source>##### Heading 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="115"/>
        <source>###### Heading 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="116"/>
        <source>*Emphasis* _Emphasis_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="117"/>
        <source>**Strong** __Strong__</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="118"/>
        <source>1. Numbered List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="119"/>
        <source>* Bullet List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="120"/>
        <source>+ Bullet List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="121"/>
        <source>- Bullet List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="122"/>
        <source>&gt; Block Quote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="123"/>
        <source>`Code Span`</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="124"/>
        <source>``` Code Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="125"/>
        <source>[Link](http://url.com &quot;Title&quot;]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="126"/>
        <source>[Reference Link][ID]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="127"/>
        <source>![Image][./image.jpg &quot;Title&quot;]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="128"/>
        <source>--- *** ___ Horizontal Rule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="131"/>
        <source>Cheat Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="687"/>
        <source>Insert Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="690"/>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="691"/>
        <source>All Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="728"/>
        <source>Tabulation Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="729"/>
        <source>Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="784"/>
        <source>Failed to open Quick Reference Guide.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="800"/>
        <source>Quick Reference Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="852"/>
        <source>&lt;p&gt;Copyright &amp;copy; 2014-2016 wereturtle&lt;/b&gt;&lt;p&gt;You may use and redistribute this software under the terms of the &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;GNU General Public License Version 3&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Visit the official website at &lt;a href=&quot;http://github.com/wereturtle/ghostwriter&quot;&gt;http://github.com/wereturtle/ghostwriter&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Special thanks and credit for reused code goes to&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;mailto:graeme@gottcode.org&quot;&gt;Graeme Gott&lt;/a&gt;, author of &lt;a href=&quot;http://gottcode.org/focuswriter/&quot;&gt;FocusWriter&lt;/a&gt;&lt;br/&gt;Dmitry Shachnev, author of &lt;a href=&quot;http://sourceforge.net/p/retext/home/ReText/&quot;&gt;Retext&lt;/a&gt;&lt;br/&gt;&lt;a href=&quot;mailto:gabriel@teuton.org&quot;&gt;Gabriel M. Beddingfield&lt;/a&gt;, author of &lt;a href=&quot;http://www.teuton.org/~gabriel/stretchplayer/&quot;&gt;StretchPlayer&lt;/a&gt;&lt;br/&gt;&lt;p&gt;I am also deeply indebted to &lt;a href=&quot;mailto:w.vollprecht@gmail.com&quot;&gt;Wolf Vollprecht&lt;/a&gt;, the author of &lt;a href=&quot;http://uberwriter.wolfvollprecht.de/&quot;&gt;UberWriter&lt;/a&gt;, for the inspiration he provided in creating such a beautiful Markdown editing tool.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="875"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/MainWindow.cpp" line="880"/>
        <source>%n word(s)</source>
        <translation type="unfinished">
            <numerusform>%n word</numerusform>
            <numerusform>%n words</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1032"/>
        <source>Restart required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1033"/>
        <source>Please restart the application for changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1045"/>
        <source>Hud Window Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1079"/>
        <source>Matched Characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1199"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1201"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1202"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1205"/>
        <source>Reopen Closed File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1214"/>
        <source>Clear Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1219"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1220"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1204"/>
        <source>Open &amp;Recent...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1221"/>
        <source>R&amp;ename...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1222"/>
        <source>Re&amp;load from Disk...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1224"/>
        <source>Print Pre&amp;view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1225"/>
        <source>&amp;Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1227"/>
        <source>&amp;Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1229"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1231"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1232"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1233"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1235"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1236"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1237"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1239"/>
        <source>&amp;Insert Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1241"/>
        <source>&amp;Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1242"/>
        <source>Rep&amp;lace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1244"/>
        <source>&amp;Spell check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1246"/>
        <source>For&amp;mat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1247"/>
        <source>&amp;Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1248"/>
        <source>&amp;Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1249"/>
        <source>Stri&amp;kthrough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1250"/>
        <source>&amp;HTML Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1252"/>
        <source>I&amp;ndent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1253"/>
        <source>&amp;Unindent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1255"/>
        <source>Block &amp;Quote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1256"/>
        <source>&amp;Strip Block Quote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1258"/>
        <source>&amp;* Bullet List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1259"/>
        <source>&amp;- Bullet List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1260"/>
        <source>&amp;+ Bullet List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1262"/>
        <source>1&amp;. Numbered List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1263"/>
        <source>1&amp;) Numbered List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1265"/>
        <source>&amp;Task List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1266"/>
        <source>Toggle Task(s) &amp;Complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1268"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1270"/>
        <source>&amp;Full Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1277"/>
        <source>&amp;Preview in HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1278"/>
        <source>&amp;Outline HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1279"/>
        <source>&amp;Cheat Sheet HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1282"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1283"/>
        <source>Themes...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1284"/>
        <source>Font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1286"/>
        <source>Focus Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1291"/>
        <source>Sentence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1297"/>
        <source>Current Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1303"/>
        <source>Three Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1309"/>
        <source>Paragraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1322"/>
        <source>Editor Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1326"/>
        <source>Narrow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1332"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1338"/>
        <source>Wide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1344"/>
        <source>Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1357"/>
        <source>Blockquote Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1361"/>
        <source>Plain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1367"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1373"/>
        <source>Fancy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1386"/>
        <source>Hide menu bar in full screen mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1393"/>
        <source>Use Large Headings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1400"/>
        <source>Use Underline Instead of Italics for Emphasis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1407"/>
        <source>Automatically Match Characters while Typing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1413"/>
        <source>Customize Matched Characters...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1416"/>
        <source>Cycle Bullet Point Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1423"/>
        <source>Display Current Time in Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1431"/>
        <source>Live Spellcheck Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1437"/>
        <source>Dictionaries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1439"/>
        <source>Locale...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1443"/>
        <source>Remember File History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1449"/>
        <source>Auto Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1455"/>
        <source>Backup File on Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1464"/>
        <source>Insert Spaces for Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1471"/>
        <source>Tabulation Width...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1476"/>
        <source>Alternate Row Colors in HUD Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1484"/>
        <source>HUD Window Button Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1488"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1494"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1505"/>
        <source>Enable Desktop Compositing Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1513"/>
        <source>HUD Window Opacity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1515"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1516"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1517"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1518"/>
        <source>Quick &amp;Reference Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1561"/>
        <source>Focus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1563"/>
        <source>Toggle distraction free mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="1571"/>
        <source>Toggle full screen mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../src/MarkdownEditor.cpp" line="125"/>
        <source>Add word to dictionary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MarkdownEditor.cpp" line="126"/>
        <source>Check spelling...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/MarkdownEditor.cpp" line="548"/>
        <source>No spelling suggestions found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/CommandLineExporter.cpp" line="92"/>
        <location filename="../src/CommandLineExporter.cpp" line="96"/>
        <source>Export failed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CommandLineExporter.cpp" line="114"/>
        <source>%1 format is not supported by this processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/CommandLineExporter.cpp" line="122"/>
        <source>Failed to execute command: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="52"/>
        <source>Markdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="53"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="54"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/DocumentManager.cpp" line="835"/>
        <source>Null or empty file path provided for writing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Exporter.cpp" line="66"/>
        <source>Export to HTML is not supported with this processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SundownExporter.cpp" line="127"/>
        <source>%1 format is unsupported by the Sundown processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="147"/>
        <source>The specified theme does not exist in the file system: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="252"/>
        <source>The specified theme is not available.  Try restarting the application.  If problem persists, please file a bug report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="322"/>
        <source>Could not delete %1 from theme.  Please try setting the theme file permissions to be writeable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="345"/>
        <source>&apos;%1&apos; already exists.  Please choose another name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="355"/>
        <source>&apos;%1&apos; theme already exists.  Please choose another name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="390"/>
        <source>Failed to rename theme.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="435"/>
        <source>Theme is read-only.  Please try renaming the theme, or setting the theme file to be writable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="472"/>
        <source>Failed to remove old theme image.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="504"/>
        <source>The old theme image file could not be removed from the theme directory.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="516"/>
        <source>Theme image file could not be copied to the theme directory.  Please check file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="535"/>
        <source>Theme could not be saved to disk.  Please check file permissions or try renaming the theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="560"/>
        <source>Untitled 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="573"/>
        <source>Untitled %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="629"/>
        <location filename="../src/ThemeFactory.cpp" line="651"/>
        <location filename="../src/ThemeFactory.cpp" line="679"/>
        <source>Invalid or missing value for %1 provided.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeFactory.cpp" line="659"/>
        <source>Value for %1 is out of range.  Valid values are between %2 and %3, inclusive.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimpleFontDialog</name>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="44"/>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="72"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="75"/>
        <source>AaBbCcXxYyZz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SimpleFontDialog.cpp" line="79"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpellChecker</name>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="139"/>
        <source>Check Spelling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="147"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="150"/>
        <source>&amp;Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="153"/>
        <source>I&amp;gnore All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="158"/>
        <source>&amp;Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="161"/>
        <source>C&amp;hange All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="176"/>
        <source>Not in dictionary:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="184"/>
        <source>Change to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="200"/>
        <source>Checking spelling...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="200"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="201"/>
        <source>Please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="228"/>
        <source>Continue checking at beginning of file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/spelling/spell_checker.cpp" line="287"/>
        <source>Spell check complete.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StyleSheetManagerDialog</name>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="38"/>
        <source>Custom Style Sheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="65"/>
        <source>Add new style sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="67"/>
        <source>Remove selected style sheet(s) from list. (No files will be deleted from the hard disk.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="113"/>
        <source>Select CSS File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="115"/>
        <source>CSS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="115"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/StyleSheetManagerDialog.cpp" line="144"/>
        <source>Don&apos;t worry! No files will be deleted from the hard disk. But are you sure you wish to remove the selected style sheet(s) from the list?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextDocument</name>
    <message>
        <location filename="../src/TextDocument.cpp" line="36"/>
        <location filename="../src/TextDocument.cpp" line="68"/>
        <source>untitled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ThemeEditorDialog</name>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="40"/>
        <source>Edit Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="98"/>
        <source>Rounded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="99"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="103"/>
        <location filename="../src/ThemeEditorDialog.cpp" line="111"/>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="104"/>
        <location filename="../src/ThemeEditorDialog.cpp" line="110"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="108"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="109"/>
        <source>Tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="112"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="113"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="124"/>
        <source>Theme Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="137"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="138"/>
        <source>Markup Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="139"/>
        <source>Link Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="140"/>
        <source>Spelling Error Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="141"/>
        <source>Text Area Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="144"/>
        <source>Text Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="150"/>
        <source>Background Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="151"/>
        <source>Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="153"/>
        <source>Editor Aspect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="154"/>
        <source>Editor Corners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="155"/>
        <source>Editor Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="158"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="164"/>
        <source>HUD Foreground Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="165"/>
        <source>HUD Background Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="168"/>
        <source>HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="247"/>
        <source>Unable to save theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeEditorDialog.cpp" line="265"/>
        <source>Failed to rename theme.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ThemeSelectionDialog</name>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="50"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="96"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="158"/>
        <source>Unable to load theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="239"/>
        <source>Cannot delete theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="240"/>
        <source>Sorry, this is a built-in theme that cannot be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="251"/>
        <source>Are you sure you want to permanently delete the &apos;%1&apos; theme?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="269"/>
        <source>Failed to delete theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="300"/>
        <source>Cannot edit theme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ThemeSelectionDialog.cpp" line="301"/>
        <source>Sorry, this is a built-in theme that cannot be edited.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
