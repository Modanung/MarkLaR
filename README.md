![MarkLaR](https://github.com/Modanung/MarkLaR/raw/master/resources/images/logo.png)

# About

*MarkLaR* is a FOSS text editor for Markdown, which is a plain text markup format created by John Gruber and [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz). For more information about Markdown, please visit [John Gruber’s website](http://www.daringfireball.net). *MarkLaR* started as a fork of [ghostwriter](https://github.com/wereturtle/ghostwriter) and is furthermore inspired by [MarkRight](https://github.com/dvcrn/markright).

![Screenshot of MarkLaR](https://raw.githubusercontent.com/Modanung/MarkLaR/master/resources/images/screenshot.png)

### Features

* Syntax highlighting of Markdown
* Navigation of document headings
* An easily toggled and frequently updated HTML preview
* Full-screen mode
* Use of custom CSS style sheets for HTML preview and printing to file or paper
* Style sheets included for GitHub, [diaspora\*](https://diasporafoundation.org/) and [Loomio](https://www.loomio.org)
* A Live word count
* Two built-in editor themes, one dark and one light
* Image URL insertion via dragging and dropping an image file into the editor
* Spell checking with Hunspell
* [Sundown](http://github.com/vmg/sundown/) processor built in for preview and export to HTML
* Single window layout

### Roadmap

* Add table generator
* Add Emoji support [:four_leaf_clover:](https://github.com/LucKeyProductions)
* Save last used style sheet choice for recent files
* Add ability to switch between editing .css and .md
* Add StackExchange to built-in style sheets

-----------------------

## Build

To build from the source code, you will need either Qt 4.8 or Qt 5, available from <http://www.qt.io/> if you are on Mac OS X, or in your Linux distribution's repository. This documentation assumes you already have the source code unzipped in a folder.

### Linux

Before proceeding, ensure that you have the following packages installed for Qt 5:

* qt5-default,
* qtbase5-dev,
* libqt5svg5-dev,
* qtmultimedia5-dev,
* libqt5webkit5-dev,
* libhunspell-dev,
* pkg-config
* libqt5concurrent5
* libqt5printsupport5
* libqt5svg5

Next, open a terminal window, and enter the following commands:

    $ cd <your_MarkLaR_folder_location>
    $ qmake
    $ make
    $ make install

The last command will install *MarkLaR* on your machine.  If you need to install the application in an alternative location to `/usr/local`, enter the following command in lieu of the second command above, passing in the desired value for `PREFIX`:

    $ qmake PREFIX=<your_install_path_here>

For example, to install under `/opt`, you would enter:

    $ qmake PREFIX=/opt

### Mac OS X

Install [homebrew](http://brew.sh).  In a terminal:

``` shell
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
Then:

``` shell
$ brew install qt qt5
$ qmake -spec macx-g++
$ make
```
If you see:

```
fatal: Not a git repository (or any of the parent directories): .git
```
Make sure you're cloned the repo, not just downloaded the src tarball.

If you want MarkLaR in your applications folder, from the repo root do:

``` shell
$ sudo cp ./build/release/marklar.app /Applications
```
To use MarkLaR from the command line (assuming `/usr/local/bin` is in your path and you've moved MarkLaR to the `/Applications` folder):

``` shell
$ sudo ln -s /Applications/MarkLaR.app/Contents/MacOS/MarkLaR /usr/local/bin
```

## Command Line Usage

For terminal users, *MarkLaR* can be run from the command line.  In your terminal window, simply type the following:

    $ marklar myfile.md

where `myfile.md` is the path to your Markdown text file.

-----------------------

> ## Licensing
> The source code for *MarkLaR* is licensed under the [GNU General Public License Version 3](http://www.gnu.org/licenses/gpl.html).  However, various icons and third-party FOSS code (i.e., Hunspell and Sundown) have different licenses compatible with GPLv3.  Please read the COPYING files in the respective folders for the different licenses.
