################################################################################
# Copyright (C) 2016 Modanung
# Copyright (C) 2014-2016 wereturtle
# Copyright (C) 2009, 2010, 2011, 2012, 2013, 2014 Graeme Gott <graeme@gottcode.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################

QMAKE_CXXFLAGS += -std=c++14

lessThan(QT_VERSION, 4.8) {
    error("MarkLaR requires Qt 4.8 or greater")
}
macx:greaterThan(QT_MAJOR_VERSION, 4):lessThan(QT_VERSION, 5.2) {
    error("MarkLaR requires Qt 5.2 or greater")
}

TEMPLATE = app
greaterThan(QT_MAJOR_VERSION, 4) { # QT v. 5
    QT += printsupport webkitwidgets widgets concurrent
}
else { # QT v. 4
    QT += webkit concurrent
}
CONFIG -= debug
CONFIG += warn_on

# Set program version
VERSION = $$system(git describe --tags)
isEmpty(VERSION) {
    VERSION = v1.4.1
}
DEFINES += APPVERSION='\\"$${VERSION}\\"'


CONFIG(debug, debug|release) {
    DESTDIR = build/debug
}
else {
    DESTDIR = build/release
}

#DEFINES += QT_NO_DEBUG_OUTPUT=1
OBJECTS_DIR = $${DESTDIR}
MOC_DIR = $${DESTDIR}
RCC_DIR = $${DESTDIR}
UI_DIR = $${DESTDIR}

TARGET = marklar

# Input

macx {
    QMAKE_INFO_PLIST = resources/Info.plist

	LIBS += -lz -framework AppKit

	HEADERS += src/spelling/dictionary_provider_nsspellchecker.h

	OBJECTIVE_SOURCES += src/spelling/dictionary_provider_nsspellchecker.mm
} else:unix {
	CONFIG += link_pkgconfig

	HEADERS += src/spelling/dictionary_provider_hunspell.h \
		src/spelling/dictionary_provider_voikko.h

	SOURCES += src/spelling/dictionary_provider_hunspell.cpp \
		src/spelling/dictionary_provider_voikko.cpp
}

INCLUDEPATH += src src/spelling

HEADERS += src/MainWindow.h \
    src/MarkdownEditor.h \
    src/Token.h \
    src/ExportFormat.h \
    src/Exporter.h \
    src/Theme.h \
    src/ThemeFactory.h \
    src/CommandLineExporter.h \
    src/TextBlockData.h \
    src/ThemeSelectionDialog.h \
    src/ThemePreviewer.h \
    src/ThemeEditorDialog.h \
    src/ExporterFactory.h \
    src/ColorHelper.h \
    src/AppSettings.h \
    src/DocumentManager.h \
    src/TextDocument.h \
    src/DocumentHistory.h \
    src/ExportDialog.h \
    src/Outline.h \
    src/MarkdownStates.h \
    src/MarkdownHighlighter.h \
    src/MessageBoxHelper.h \
    src/GraphicsFadeEffect.h \
    src/SundownExporter.h \
    src/StyleSheetManagerDialog.h \
    src/SimpleFontDialog.h \
    src/HighlighterLineStates.h \
    src/HighlightTokenizer.h \
    src/MarkdownTokenizer.h \
    src/EffectsMenuBar.h \
    src/LocaleDialog.h \
    src/find_dialog.h \
    src/image_button.h \
    src/color_button.h \
    src/spelling/abstract_dictionary.h \
    src/spelling/abstract_dictionary_provider.h \
    src/spelling/dictionary_dialog.h \
    src/spelling/dictionary_manager.h \
    src/spelling/dictionary_ref.h \
    src/spelling/spell_checker.h \
    src/spelling/hunspell/affentry.hxx \
    src/spelling/hunspell/affixmgr.hxx \
    src/spelling/hunspell/atypes.hxx \
    src/spelling/hunspell/baseaffix.hxx \
    src/spelling/hunspell/csutil.hxx \
    src/spelling/hunspell/dictmgr.hxx \
    src/spelling/hunspell/filemgr.hxx \
    src/spelling/hunspell/hashmgr.hxx \
    src/spelling/hunspell/htypes.hxx \
    src/spelling/hunspell/hunspell.hxx \
    src/spelling/hunspell/hunzip.hxx \
    src/spelling/hunspell/langnum.hxx \
    src/spelling/hunspell/phonet.hxx \
    src/spelling/hunspell/replist.hxx \
    src/spelling/hunspell/suggestmgr.hxx \
    src/spelling/hunspell/w_char.hxx \
    src/sundown/autolink.h \
    src/sundown/buffer.h \
    src/sundown/houdini.h \
    src/sundown/html_blocks.h \
    src/sundown/html.h \
    src/sundown/markdown.h \
    src/sundown/stack.h \
    src/HtmlPreview.h \
    src/SpellBook.h

SOURCES += src/AppMain.cpp \
    src/MainWindow.cpp \
    src/MarkdownEditor.cpp \
    src/Token.cpp \
    src/Exporter.cpp \
    src/ExportFormat.cpp \
    src/Theme.cpp \
    src/ThemeFactory.cpp \
    src/CommandLineExporter.cpp \
    src/ThemeSelectionDialog.cpp \
    src/ThemePreviewer.cpp \
    src/ThemeEditorDialog.cpp \
    src/ExporterFactory.cpp \
    src/ColorHelper.cpp \
    src/AppSettings.cpp \
    src/DocumentManager.cpp \
    src/TextDocument.cpp \
    src/DocumentHistory.cpp \
    src/ExportDialog.cpp \
    src/Outline.cpp \
    src/MarkdownHighlighter.cpp \
    src/MessageBoxHelper.cpp \
    src/GraphicsFadeEffect.cpp \
    src/StyleSheetManagerDialog.cpp \
    src/SimpleFontDialog.cpp \
    src/SundownExporter.cpp \
    src/HighlightTokenizer.cpp \
    src/MarkdownTokenizer.cpp \
    src/EffectsMenuBar.cpp \
    src/LocaleDialog.cpp \
    src/find_dialog.cpp \
    src/image_button.cpp \
    src/color_button.cpp \
    src/spelling/dictionary_dialog.cpp \
    src/spelling/dictionary_manager.cpp \
    src/spelling/spell_checker.cpp \
    src/spelling/hunspell/affentry.cxx \
    src/spelling/hunspell/affixmgr.cxx \
    src/spelling/hunspell/csutil.cxx \
    src/spelling/hunspell/dictmgr.cxx \
    src/spelling/hunspell/filemgr.cxx \
    src/spelling/hunspell/hashmgr.cxx \
    src/spelling/hunspell/hunspell.cxx \
    src/spelling/hunspell/hunzip.cxx \
    src/spelling/hunspell/phonet.cxx \
    src/spelling/hunspell/replist.cxx \
    src/spelling/hunspell/suggestmgr.cxx \
    src/spelling/hunspell/utf_info.cxx \
    src/sundown/autolink.c \
    src/sundown/buffer.c \
    src/sundown/houdini_href_e.c \
    src/sundown/houdini_html_e.c \
    src/sundown/html_smartypants.c \
    src/sundown/html.c \
    src/sundown/markdown.c \
    src/sundown/stack.c \
    src/HtmlPreview.cpp \
    src/SpellBook.cpp

# Allow for updating translations
TRANSLATIONS = $$files(translations/marklar_*.ts)

RESOURCES += resources.qrc

macx {
    ICON = resources/mac/marklar.icns
} else:unix {
    isEmpty(PREFIX) {
        PREFIX = /usr/local
    }
    isEmpty(BINDIR) {
        BINDIR = $$PREFIX/bin
    }
    isEmpty(DATADIR) {
        DATADIR = $$PREFIX/share
    }
    DEFINES += DATADIR=\\\"$${DATADIR}/marklar\\\"

    target.path = $$BINDIR

    pixmap.files = resources/linux/icons/marklar.xpm
    pixmap.path = $$DATADIR/pixmaps

    icon.files = resources/linux/icons/hicolor/*
    icon.path = $$DATADIR/icons/hicolor

    desktop.files = resources/linux/marklar.desktop
    desktop.path = $$DATADIR/applications/

    appdata.files = resources/linux/marklar.appdata.xml
    appdata.path = $$DATADIR/appdata/

    man.files = resources/linux/marklar.1
    man.path = $$PREFIX/share/man/man1

    qm.files = translations/*.qm
    qm.path = $$DATADIR/marklar/translations

    INSTALLS += target icon pixmap desktop appdata man icon qm
}
